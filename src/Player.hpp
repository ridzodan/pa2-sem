#pragma once

/** Class saves player progress in the game */
class Player{
    public:

        Player();
        ~Player();

        double getMoney() const;

        void setMoney(double money);

        void addMoney(double amount);

        int getHealth() const;

        void substractHealth(int val);

        void setHealth(int health);

        void setEnemiesSpawned(int enemiesSpawned);

        void addEnemySpawned();

        int getEnemiesSpawned() const;

        void addToScore(int amount);

        int getScore() const;

        bool isDead() const;

    private:

        double money;
        int health;
        int enemiesSpawned = 0; /**< how many enemies have been spawned in this game */
        int score = 0;
};
