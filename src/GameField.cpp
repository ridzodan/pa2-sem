#include <iostream>

#include "GameField.hpp"

using namespace std;


GameField::GameField(){
}

GameField::GameField(string fileName, int difficulty)
    : mapName(fileName), difficulty(difficulty){
}

bool GameField::loadBoard(const string & filename){

    // Check if save has valid checksum
    if(! LevelEditor::checkChecksum(filename)){
        return false;
    }

    ifstream inStream;
    inStream = ifstream(filename);
    if(!inStream || ! inStream.is_open()){
        return false;
    }

    // Loading game field dimensions
    if(!(inStream >> width >> height)){
        inStream.close();
        return false;
    }

    // Check screen size
    if((width * 3) + LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH - COLS > 0 || (height * 3) + 1 - LINES > 0){
        inStream.close();
        return false;
    }

    // Create ncurses windows
    window = newwin((height * 3), (width * 3), TOP_OFFSET, LEFT_PANEL_WIDTH + 1);
    wrefresh(window);


    leftPanel = newwin((height * 3), LEFT_PANEL_WIDTH, TOP_OFFSET, 0);
    box(leftPanel, 0, 0);
    wrefresh(leftPanel);


    rightPanel = newwin((height * 3), RIGHT_PANEL_WIDTH, TOP_OFFSET, LEFT_PANEL_WIDTH + width * 3 + 2);
    box(rightPanel, 0 , 0);
    wrefresh(rightPanel);

    // Initialize tower rating map
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            mapRating[Coordinate(x, y)] = 1;
        }
    }

    // Make walls around screen (They dont have to be in map file).
    for (int i = 0; i < width; i++){
        addTower(new TowerPlace(Coordinate(i, 0), WALL_ID));
        addTower(new TowerPlace(Coordinate(i, height - 1), WALL_ID));
    }
    for (int j = 1; j < height - 1; j++){
        addTower(new TowerPlace(Coordinate(0, j), WALL_ID));
        addTower(new TowerPlace(Coordinate(width - 1, j), WALL_ID));
    }


    // Loading game objects
    char type;
    while(! inStream.eof()){
        inStream >> type;

        //checking blank line (end of file)
        if(!inStream || inStream.eof()){
            break;
        }

        switch(type){
            case 'W':
                loadWall(inStream);
                break;
            case 'T':
                loadPlaceForTower(inStream);
                break;
            case 'S':
                loadSpawn(inStream);
                break;
            case 'B':
                if( base != NULL ){
                    destruct();
                    return false;
                }
                loadBase(inStream);
                break;
            default:
                break;
        }
    }

    // Check if spawn has been loaded and bas has been loaded
    if(spawns.size() == 0 || base == NULL){
        destruct();
        return false;
    }

    processDifficulty();

    return true;
}

bool GameField::loadSave(const string & filename){

    // Check if save has valid checksum
    if(! LevelEditor::checkChecksum(filename)){
        return false;
    }

    ifstream inStream(filename);
    if(!inStream || !inStream.is_open()){
        return false;
    }

    // Load game info
    int health, score;
    double money, enemiesSpawned;

    inStream >> mapName >> difficulty >> width >> height >> health;
    inStream >> money >> enemiesSpawned >> score;

    processDifficulty();

    // Check screen size
    if((width * 3) + LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH - COLS > 0 || (height * 3) + 1 - LINES > 0){
        inStream.close();
        return false;
    }

    this->player.setHealth(health);
    this->player.setMoney(money);
    this->player.setEnemiesSpawned(enemiesSpawned);
    this->player.addToScore(score);

    // Set up ncurses windows
    window = newwin((height * 3), (width * 3), TOP_OFFSET, LEFT_PANEL_WIDTH + 1);
    wrefresh(window);

    leftPanel = newwin((height * 3), LEFT_PANEL_WIDTH, TOP_OFFSET, 0);
    box(leftPanel, 0, 0);
    wrefresh(leftPanel);

    rightPanel = newwin((height * 3), RIGHT_PANEL_WIDTH, TOP_OFFSET, LEFT_PANEL_WIDTH + width * 3 + 2);
    box(rightPanel, 0 , 0);
    wrefresh(rightPanel);

    // Initialize tower rating map
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            mapRating[Coordinate(x, y)] = 1;
        }
    }

    // Load towers and enemies
    unsigned id;
    while(inStream >> id){
        if(id >= 20){
            Enemy * enemy = loadEnemy(inStream, id);
            enemies.push_back(enemy);
        }
        else{
            Tower * tower = loadTower(inStream, id);
            addTower(tower);
        }
    }
    for(Enemy * enemy : enemies){
        enemy->generatePath(this);
    }
    inStream.close();
    return true;

}

int GameField::getWidth() const{
    return width;
}

int GameField::getHeight() const{
    return height;
}

Player & GameField::getPlayer(){
    return player;
}

WINDOW * GameField::getWindow() const{
    return window;
}

WINDOW * GameField::getLeftPanel() const{
    return leftPanel;
}

WINDOW * GameField::getRightPanel() const{
    return rightPanel;
}

void GameField::render(char c){

    clearWindow();
    renderWindow();

    clearLeftPanel();
    renderLeftPanel(c);

    clearRightPanel();
    renderRightPanel();
}

void GameField::buyTower(Coordinate pos, char type){

    // Check if x, y is valid position for building new tower
    auto it = towersMap.find(pos);
    if(it == towersMap.end()){
        return;
    }
    Tower * place = towersMap[pos];
    if(!place->IsBuildable()){
        return;
    }

    // Check if player has enough money
    switch(type){
        case 'D':
            if(player.getMoney() >= DESTROYER_PRICE){
                player.setMoney(player.getMoney() - DESTROYER_PRICE);
                addTower(new Destroyer(pos, DESTROYER_RANGE, DESTROYER_DMG, DESTROYER_RELOAD));
            }
            return;
        case 'S':
            if(player.getMoney() >= SNIPER_PRICE){
                player.setMoney(player.getMoney() - SNIPER_PRICE);
                addTower( new Sniper(pos, SNIPER_RANGE, SNIPER_DMG, SNIPER_RELOAD));
            }
            return;

        default:
            return;
    }
}

void GameField::increaseReloads(){

    currentSpawnReload ++;

    for(auto tower : towersMap){
        tower.second->increaseReload();
    }

    for(Enemy * enemy : enemies){
        enemy->increaseMoveReload();
    }
}

void GameField::spawnEnemy(){

    // Check cooldown
    if(currentSpawnReload < spawnReload){
        return;
    }
    currentSpawnReload = 0;

    if(spawns.size() == 0){
        return;
    }

    // Add spawned enemy to stats.
    player.addEnemySpawned();

    // Choose random spawner.
    int index = rand() % spawns.size();
    Coordinate pos = spawns[index]->getPos();

    Enemy * enemy = NULL;
    double healthCoefficient = pow(healthCoeficient, player.getEnemiesSpawned());

    // Decide whether to spawn a tank.
    if(rand() % tankChance == 1 && player.getEnemiesSpawned() >= firstTank){
        enemy = new Tank(pos, TANK_SPEED, TANK_HEALTH * healthCoeficient, TANK_REWARD);
    }
    else{
        // Choose random enemy type
        int type = rand() % 2;

        switch(type){
            case 0:
                enemy = new Sneaker(pos, SNEAKER_SPEED, SNEAKER_HEALTH * healthCoefficient, SNEKAER_REWARD);
                break;
            case 1:
            default:
                enemy = new Puncher(pos, PUNCHER_SPEED, PUNCHER_HEALTH * healthCoefficient, PUNCHER_REWARD);
                break;
        }

    }

    enemy->generatePath(this);
    enemies.push_back(enemy);
}

void GameField::moveEnemies(){

    for(Enemy * enemy: enemies){
        enemy->move();
    }
}

TowerPlace * GameField::getBase() const{
    return base;
}

Tower * GameField::getTowerAt(Coordinate pos) const{

    auto it = towersMap.find(pos);
    if(it == towersMap.end()){
        return NULL;
    }

    return towersMap.at(pos);
}

void GameField::checkEnemies(){

    for(int i = enemies.size() - 1; i >= 0; i--){

        // Check if enemy has reached player base
        bool reachedBase = enemies[i]->getPos() == base->getPos();
        if(reachedBase){
            player.substractHealth(1);
        }

        // Check if enemy is dead
        if(enemies[i]->getHealth() <= 0){
            player.addMoney(enemies[i]->getReward());
            player.addToScore(enemies[i]->getReward() * scoreCoeficient);
        }

        // Delete enemy and references to itself
        if(reachedBase || enemies[i]->getHealth() <= 0 || enemies[i]->isPathEmpty()){
            for(auto it: towersMap){
                Tower * tower = it.second;
                if(tower->getTarget() == enemies[i]){
                    tower->setTarget(NULL);
                }
            }
            delete enemies[i];
            enemies.erase(enemies.begin() + i);
        }
    }
}

void GameField::towersShoot(){

    for(auto tower: towersMap){
        tower.second->shoot(this);
    }
}

const vector<Enemy *> & GameField::getEnemies() const{
    return enemies;
}

void GameField::saveGame(){

    string filename = string(SAVES_DIR) + "/save";
    int suffix = 1;

    // Find first available file to create save
    while(true){
        ifstream inStream;
        stringstream tmp;
        tmp << filename << suffix << ".save";
        inStream = ifstream(tmp.str());
        if(!inStream){
            break;
        }
        inStream.close();
        suffix ++;
    }

    stringstream outputFileName;
    outputFileName << "save" << suffix << ".save";

    saveGame(outputFileName.str());

}

double GameField::getRatingAt(Coordinate pos) const{

    auto it = mapRating.find(pos);
    if(it == mapRating.end()){
        return 0;
    }

    return mapRating.at(pos);
}

void GameField::setBuildMode(){
    gameState = 'B';
}

bool GameField::isBuildMode() const {
    return (gameState == 'B');
}

void GameField::setPlayMode(){
    gameState = 'G';
}

bool GameField::isPlayMode() const {
    return (gameState == 'G');
}

void GameField::setUpgradeMode(){
    gameState = 'U';
}

bool GameField::isUpgradeMode() const {
    return (gameState == 'U');
}

void GameField::destruct() {
    for(auto tower: towersMap){
        delete tower.second;
    }
    for(Enemy * enemy: enemies){
        delete enemy;
    }

    clearLeftPanel();
    wrefresh(leftPanel);
    delwin(leftPanel);

    clearWindow();
    wrefresh(window);
    delwin(window);

    clearRightPanel();
    wrefresh(rightPanel);
    delwin(rightPanel);
}

void GameField::loadWall(ifstream & inStream){

    int x, y;
    if(!(inStream >> x >> y) || !checkCoordinate(Coordinate(x, y))){
        return;
    }
    addTower(new TowerPlace(Coordinate(x, y), WALL_ID));
    return;
}

void GameField::loadPlaceForTower(ifstream & inStream){

    int x, y;
    if(!(inStream >> x >> y) || !checkCoordinate(Coordinate(x, y))){
        return;
    }
    addTower(new TowerPlace(Coordinate(x, y), TOWER_BASE_ID));
    return;
}

void GameField::loadBase(ifstream & inStream){

    int x, y;
    if(!(inStream >> x >> y) || !checkCoordinate(Coordinate(x, y))){
        return;
    }
    base = new TowerPlace(Coordinate(x, y), BASE_ID);
    addTower(base);
    return;
}

void GameField::loadSpawn(ifstream & inStream){

    int x, y;
    if(!(inStream >> x >> y) || !checkCoordinate(Coordinate(x, y))){
        return;
    }
    TowerPlace * tmp = new TowerPlace(Coordinate(x, y), SPAWN_ID);
    addTower(tmp);
    spawns.push_back(tmp);
    return;
}

bool GameField::checkCoordinate(Coordinate pos) const{

    if(pos.getX() <= 0 || pos.getX() >= width - 1){
        return false;
    }
    if(pos.getY() <= 0 || pos.getY() >= height - 1){
        return false;
    }
    return true;
}

void GameField::renderLeftPanel(char c){

    box(leftPanel, 0, 0);

    // Print last pressed key (Just for debugging)
    char * s = (char *) keyname(c);
    mvwaddstr (leftPanel, 1, 1, "Key presed:");
    if (s)
        mvwprintw (leftPanel, 3, 1, "'%s %d'", s, c);
    else
        mvwprintw (leftPanel, 3, 1, "'%c'", (isprint(c) ? c : '.'));

    mvwprintw(leftPanel, 6, 1, "Money: %.2lf $", player.getMoney());
    mvwprintw(leftPanel, 8, 1, "HP:    %d", player.getHealth());
    mvwprintw(leftPanel, 10, 1, "Score: %d", player.getScore());

    switch(difficulty){
        case 0:
            mvwprintw(leftPanel, 12, 1, "EASY");
            break;
        case 1:
            mvwprintw(leftPanel, 12, 1, "NORMAL");
            break;
        case 2:
            mvwprintw(leftPanel, 12, 1, "HARD");
            break;
        case 3:
            mvwprintw(leftPanel, 12, 1, "HARDCORE");
            break;
        default:
            break;
    }

    wrefresh(leftPanel);
}

void GameField::renderRightPanel(){

    box(rightPanel, 0, 0);

    if ( isPlayMode() ){
        mvwprintw(rightPanel, 1, 1, "GAME");
        mvwprintw(rightPanel, 2, 1, "----------------------------");
        mvwprintw(rightPanel, 4, 1, "q/Esc - exit");
        mvwprintw(rightPanel, 6, 1, "b - build mode");
        mvwprintw(rightPanel, 8, 1, "s - save and quit");
    }
    else if ( isBuildMode() ){
        mvwprintw(rightPanel, 1, 1, "BUILD MODE");
        mvwprintw(rightPanel, 2, 1, "----------------------------");
        mvwprintw(rightPanel, 4, 1, "q - exit build mode");
        mvwprintw(rightPanel, 6, 1, "wsad/arrows - move cursor");

        mvwprintw(rightPanel, 8, 1,  "1 - ┌|┐ Destroyer");
        mvwprintw(rightPanel, 9, 1,  "    │D│ DMG: %d Range: %d", DESTROYER_DMG, DESTROYER_RANGE);
        mvwprintw(rightPanel, 10, 1, "    └─┘ Reload: %d Cost: %d$", DESTROYER_RELOAD, DESTROYER_PRICE);

        mvwprintw(rightPanel, 12, 1, "2 - ┌║┐ Sniper");
        mvwprintw(rightPanel, 13, 1, "    │S│ DMG: %d Range: %d", SNIPER_DMG, SNIPER_RANGE);
        mvwprintw(rightPanel, 14, 1, "    └─┘ Reload: %d Cost: %d$", SNIPER_RELOAD, SNIPER_PRICE);

    }
    else if( isUpgradeMode() ){
        mvwprintw(rightPanel, 1, 1, "UPGRADE MODE");
        mvwprintw(rightPanel, 2, 1, "----------------------------");
        mvwprintw(rightPanel, 4, 1, "q - exit upgrade mode");
        mvwprintw(rightPanel, 6, 1, "u - confirm upgrade");
        mvwprintw(rightPanel, 8, 1, "wsad/arrows - move cursor");

        mvwprintw(rightPanel, 10, 1, "DMG: %.2lf (+%.2lf)", cursorOn->getDemage(), cursorOn->getDemageUpgradeBenefits());
        mvwprintw(rightPanel, 11, 1, "Range: %.2lf (+%.2lf)", cursorOn->getRange(), cursorOn->getRangeUpgradeBenefits());
        mvwprintw(rightPanel, 12, 1, "Reload: %.2lf (-%.2lf)", cursorOn->getReload(), cursorOn->getReloadUpgradeBenefits());
        mvwprintw(rightPanel, 13, 1, "----%.2lf$----", cursorOn->getUpgradeCost());
    }

    wrefresh(rightPanel);
}

void GameField::renderWindow(){

    // Render range indicator for selected tower
    if(cursorOn != NULL){
        cursorOn->renderRangeIndicator(width, height, window);
    }

    for(auto tower : towersMap){
        tower.second->render(window);
    }
    for(Enemy * enemy : enemies){
        enemy->render(window);
    }
    wrefresh(window);
}

void GameField::clearLeftPanel(){

    // I dont use wclear, because it causes flickering
    for(int i = 0; i < LEFT_PANEL_WIDTH; i++){
        for(int k = 0; k < height * 3; k++){
            mvwaddch(leftPanel, k, i, ' ');
        }
    }

}

void GameField::clearRightPanel(){

    for(int i = 0; i < RIGHT_PANEL_WIDTH; i++){
        for(int k = 0; k < height * 3; k++){
            mvwaddch(rightPanel, k, i, ' ');
        }
    }
}

void GameField::clearWindow(){

    for(int i = 0; i < width * 3; i++){
        for(int k = 0; k < height * 3; k++){
            mvwaddch(window, k, i, ' ');
        }
    }
}

void GameField::addTower(Tower * tower){

    // If key in map exists, overwrite the value and delete prev value
    if(towersMap.find(tower->getPos()) != towersMap.end()){
        delete towersMap[tower->getPos()];
    }
    towersMap[tower->getPos()] = tower;

    // Generate new paths of enemies
    if(tower->getDemage() > 0){
        addTowerToRating(tower);
        for(Enemy * enemy : enemies){
            enemy->generatePath(this);
        }
    }
}

void GameField::addTowerToRating(Tower * tower){

    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){

            // Check if place is in range
            double distance = Coordinate::getDistance(tower->getPos(), Coordinate(x, y));
            if(distance > tower->getRange()){
                continue;
            }

            double dpt = 0;
            if (tower->getReload() > 0){
                dpt = tower->getDemage() / tower->getReload();
            }

            mapRating[Coordinate(x, y)] += dpt;

        }
    }
}

void GameField::saveGame(const string & filename){

    saveName = filename;

    stringstream filepath;
    filepath << SAVES_DIR << "/" << filename;

    ofstream outputFile;
    outputFile.open(filepath.str());

    stringstream gameStr;
    gameStr << mapName << " " << difficulty << " ";
    gameStr << width << " " << height << " " << player.getHealth() << " " << player.getMoney();
    gameStr << " " << player.getEnemiesSpawned() << " " << player.getScore();

    for(auto tower: towersMap){
        gameStr << tower.second->toString();
    }

    for(Enemy * enemy: enemies){
        gameStr << enemy->toString();
    }

    // Generate checksum for save data, to prevent user editing.
    const string tmp = gameStr.str();
    gameStr << ";" << LevelEditor::generateChecksum(tmp);

    outputFile << gameStr.str();

    outputFile.close();
}

Enemy * GameField::loadEnemy(ifstream & inStream, unsigned id) const{

    int x, y;
    double health, maxHealth, healing, moveCoooldown, currentMoveCooldown, reward;

    inStream >> x >> y;
    inStream >> health >> maxHealth >> healing >> moveCoooldown >> currentMoveCooldown >> reward;

    Enemy * res = NULL;
    Coordinate pos(x, y);
    switch (id)
    {
        case SNEAKER_ID:
            res = new Sneaker(pos, moveCoooldown, maxHealth, reward);
            res->setCurrentMoveCooldown(currentMoveCooldown);
            res->setHealth(health);
            break;

        case PUNCHER_ID:
        default:
            res = new Puncher(pos, moveCoooldown, maxHealth, reward);
            res->setCurrentMoveCooldown(currentMoveCooldown);
            res->setHealth(health);
            break;
    }

    return res;
}

Tower * GameField::loadTower(ifstream & inStream, unsigned id){

    int x, y, upgradeLevel;
    double range, demage, reloadTime, currentReload;
    Tower * res = NULL;

    inStream >> x >> y;
    inStream >> range >> demage >> reloadTime >> currentReload >> upgradeLevel;

    Coordinate pos(x, y);

    switch (id)
    {
        case TOWER_BASE_ID:
            res = new TowerPlace(pos, TOWER_BASE_ID);
            break;
        case BASE_ID:
            base = new TowerPlace(pos, BASE_ID);
            res = base;
            break;
        case SPAWN_ID:
            res = new TowerPlace(pos, SPAWN_ID);
            spawns.push_back(res);
            break;
        case DESTROYER_ID:
            res = new Destroyer(pos, range, demage, reloadTime);
            res->setCurrentReload(currentReload);
            break;
        case SNIPER_ID:
            res = new Sniper(pos, range, demage, reloadTime);
            res->setCurrentReload(currentReload);
            break;
        case WALL_ID:
        default:
            res = new TowerPlace(pos, WALL_ID);
            break;
    }

    res->setUpgradeLevel(upgradeLevel);

    return res;
}

string GameField::getMapName() const {
    return mapName;
}

string GameField::getSaveName() const {
    return saveName;
}

void GameField::setCursorOn(Tower * tower){
    cursorOn = tower;
}

void GameField::upgradeSelectedTower(){
    if(cursorOn == NULL){
        return;
    }

    if(cursorOn->getUpgradeCost() <= player.getMoney()){
        player.setMoney(player.getMoney() - cursorOn->getUpgradeCost());
        cursorOn->upgrade();
    }
    return;
}

int GameField::getDifficulty() const{
    return difficulty;
}

void GameField::processDifficulty(){

    switch (difficulty){
        case 0: // Easy
            player.setMoney(EASY_INITIAL_MONEY);
            player.setHealth(EASY_INITIAL_HEALTH);
            spawnReload = EASY_SPAWN_SPEED;
            healthCoeficient = EASY_HEALTH_COEFICIENT;
            firstTank = EASY_FIRST_TANK;
            tankChance = EASY_TANK_CHANCE;
            scoreCoeficient = EASY_SCORE_COEFICIENT;
            break;

        case 1: // Normal
            player.setMoney(NORMAL_INITIAL_MONEY);
            player.setHealth(NORMAL_INITIAL_HEALTH);
            spawnReload = NORMAL_SPAWN_SPEED;
            healthCoeficient = NORMAL_HEALTH_COEFICIENT;
            firstTank = NORMAL_FIRST_TANK;
            tankChance = NORMAL_TANK_CHANCE;
            scoreCoeficient = NORMAL_SCORE_COEFICIENT;
            break;

        case 2: // Hard
            player.setMoney(HARD_INITIAL_MONEY);
            player.setHealth(HARD_INITIAL_HEALTH);
            spawnReload = HARD_SPAWN_SPEED;
            healthCoeficient = HARD_HEALTH_COEFICIENT;
            firstTank = HARD_FIRST_TANK;
            tankChance = HARD_TANK_CHANCE;
            scoreCoeficient = HARD_SCORE_COEFICIENT;
            break;

        case 3: // Hardcore
        default:
            player.setMoney(HARD_INITIAL_MONEY);
            player.setHealth(1);
            spawnReload = HARD_SPAWN_SPEED;
            healthCoeficient = HARD_HEALTH_COEFICIENT;
            firstTank = HARD_FIRST_TANK;
            tankChance = HARD_TANK_CHANCE;
            scoreCoeficient = HARD_SCORE_COEFICIENT;
            break;
    }
}

void GameField::spawnRandomEnemyAtPos(Coordinate pos){

    // Add spawned enemy to stats.
    player.addEnemySpawned();

    Enemy * enemy = NULL;
    double healthCoefficient = pow(healthCoeficient, player.getEnemiesSpawned());

    // Choose random enemy type
    int type = rand() % 2;

    switch(type){
        case 0:
            enemy = new Sneaker(pos, SNEAKER_SPEED, SNEAKER_HEALTH * healthCoefficient, SNEKAER_REWARD);
            break;
        case 1:
        default:
            enemy = new Puncher(pos, PUNCHER_SPEED, PUNCHER_HEALTH * healthCoefficient, PUNCHER_REWARD);
            break;
    }

    enemy->generatePath(this);
    enemies.push_back(enemy);
}
