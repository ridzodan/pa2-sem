#pragma once

#include <stdio.h>
#include <ncurses.h>
#include <map>
#include <string>
#include <fstream>
#include <sstream>

#include "Config.hpp"
#include "Coordinate.hpp"

#define GREEN 2
#define RED 4

using namespace std;

/**
 * Utility to make new game maps
*/
class LevelEditor{
    public:

        LevelEditor();

        /**
         * Creates new map and saves it in maps folder
        */
        void createMap();

        /**
         * Loads existing map,
         * player can modify its objects
         * after that map is saved to a new file.
         * @param filename - path to edited file
         * @returns true if file was loaded succesfully
        */
        bool editMap(const string & filename);

        /**
         * Generates checksum from given string
         * checksum is then added at the end of each generated file to prevent user manual editation.
         * @param str - content of file, from which checksum will be computed.
        */
        static unsigned generateChecksum(const string & str);

        /**
         * Loads file and checks if checksum matches.
         * @param filename - name of file which will be checked
         * @returns bool whether file is valid (not edited)
        */
        static bool checkChecksum(const string & filename);

    private:

        int width = 0; /**< width of game field */
        int height = 0; /**< height of game field */

        int maxWidth = 0; /**< max horizontal width of the map */
        int maxHeight = 0; /**< max vertical height of the map */

        WINDOW * window = NULL; /**< ncurses WINDOW in which game editor is rendered */
        WINDOW * leftPanel = NULL; /**< ncurses WINDOW in which editor help is rendered */

        map<Coordinate, char> towers; /**< map of places where player built a tower */

        bool saveMap = true; /**< t/f whether player wants to save the map when he exits. */


        /**
         * Utility to set up area of map. Use arrows to change dimensions. validate by enter.
         * */
        void configureDimensions();

        /**
         * Renders border preview of walls around selected area.
        */
        void renderBorderPreview();

        /**
         * Renders towers which player have set up on the screen.
        */
        void renderPreview();

        /**
         * Clears main window.
        */
        void clearWindow();

        /**
         * Clears side panel window.
        */
        void clearLeftWindow();

        /**
         * Renders tower on the map.
         * @param pos - coordinate where tower will be rendered
         * @param type - type of tower which is beeing rendered
        */
        void renderTower(const Coordinate & pos, char type);

        /**
         * Process players inputb and builds towers on the map.
        */
        void createTowers();

        /**
         * Validates if map has all needed towers.
         * @returns true if map is valid.
        */
        bool validateMap();

        /**
         * Saves created map to a file
        */
        void saveMapFile();

        /**
         * Deletes ncurses windows.
        */
        void deleteWindows();

        /**
         * Laods map dimensions and towers from the file
         * @param filename - path to the map file.
         * @returns true, if file has been succefully loaded.
        */
        bool loadFile(const string & filename);
};
