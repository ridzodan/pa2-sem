#pragma once

#include <math.h>

using namespace std;

class Coordinate{

    public:
        Coordinate();
        Coordinate(int x, int y);

        void setX(int x);
        void setY(int y);

        int getX() const;
        int getY() const;

        /**
         * @returns true if both coordinates have same x and y
        */
        bool operator == (const Coordinate & c2) const;

        /**
         * @returns false if both coordinates have same x and y
        */
        bool operator != (const Coordinate & c2) const;

        /**
         * Compares coordinate hashes, so they can be map keys.
        */
        bool operator < (const Coordinate & c2) const;

        /**
         * @returns sum Coordinate of both coordinates.
        */
        Coordinate operator + (const Coordinate & c2) const;

        /**
         * @returns distance between both coordinates.
        */
        static double getDistance(const Coordinate & c1, const Coordinate & c2);


    private:
        int x = 0;
        int y = 0;

};
