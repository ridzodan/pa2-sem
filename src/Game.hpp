#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <ncurses.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream>

#include "towers/Tower.hpp"
#include "GameField.hpp"
#include "GameMenu.hpp"
#include "Coordinate.hpp"
#include "LevelEditor.hpp"

using namespace std;

#define COLOR 1

/** Class represents "interface" of the game which is accessible from the outside (main.cpp)  */
class Game{

    public:

        GameField gameField;

        Game();

        ~Game();

        /**
         * Only public fuction, that can be called from the outside
         * starts the game.
         *
         * Prepares main menu
         * Run the game
         * Closes the game
        */
        void run();

    private:

        /**
         * Cretes new GameField object and loads the map from given filename
         * @param filePath - path to a file where map is saved
         * @param fileName - name of file where map is saved
         * @param difficulty - how hard the game will be
         * @returns bool whether the map file has been succesfuly loaded.
        */
        bool loadMap(const string & filePath, const string & fileName, int difficulty);

        /**
         * Cretes new GameField object and loads map and game progress from given filename
         * @param gameSave - name of file where game progress is saved
         * @returns bool whether the save file has been succesfuly loaded.
        */
        bool loadSave(const string & gameSave);

        /**
         * Method contains main game cycle.
        */
        void play();

        /**
         * Player set game to tower build mode, now towers can be bought and built.
         * Game is paused, when this method runs.
        */
        void towerBuildMode();

        /**
         * Prepare ncurses to display the game.
        */
        bool prepareWindow();

        /**
         * Deallocates ncurses memory.
        */
        void closeWindow();

        /**
         * Shows main game menu and based on the result calls according functions.
        */
        void mainMenuHandler();

        /**
         * Shows list of maps and based on player selection
         * loads selected map.
        */
        void mapMenuHandler();

        /**
         * Shows list of saves and based on player selection
         * loads selected save.
        */
        void savesMenuHandler();

        /**
         * Shows list of help submenus and based on player selection
         * open selected help menu.
        */
        void displayHelp();

        /**
         * Displays players score after the game.
         * If player wants to save the score, score is saved.
         * @param score - score of player in the game.
         * @param filename - mapname where player made the score.
         * @param difficulty - which difficulty have player played.
        */
        void scoreSceneHandler(int score, string filename, int difficulty);

        /**
         * Displays name of save file where game progress is saved.
         * @param saveName - Name of original map (Rankings from saves need to know original map name)
        */
        void gameSavedSceneHandler(string saveName);

        /**
         * Shows list of maps and based on players choice
         * opens selected map scoreboard.
        */
        void rankingsMapChoiceHandler();

        /**
         * Shows sorted scores and player names on selected map
         * @param mapName selected map
        */
        void rankingsDisplayHandler(string mapName);

        /**
         * Show to player info, that map/save failed to load.
        */
        void loadFailedHandler();

        /**
         * Let player choose difficulty of the map.
         * @returns selected difficulty (0-easy - 2-hard)
        */
        int difficultyMenuHandler();

        /**
         * Let player choose which map he wants to edit.
        */
        void editMapMenuHandler();

        /**
         * @returns vector of files which are in given directory and have selected name format.
         * @param filePath - directory where files are searched ("src/maps")
         * @param namePrefix - common prefix of searched files ("save")
         * @param suffix - common suddix of searched files ("map")
        */
        vector<string> getFileChoices(string filePath, string namePrefix, string suffix);

        /**
         * @returns vector of scores from selected map, if file is corrupted returns empty vector
         * @param mapName - name of selected map
        */
        vector<string> getScores(string mapName);

        /**
         * Compares 2 game scores
         * @returns true if a > b
         * @param a - pair of score and player name
         * @param b - pair of score and player name
        */
        static bool compareResults(pair<int, string> & a, pair<int, string> & b);

        /**
         * Calls level editor to create a new map.
        */
        void createMap();

        /**
         * Display selected help page
         * @param page - selected page of help.
        */
        void displayHelpPage(int page);
};
