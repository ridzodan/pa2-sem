#include "LevelEditor.hpp"

LevelEditor::LevelEditor(){

    maxWidth = (COLS - LEFT_PANEL_WIDTH - RIGHT_PANEL_WIDTH) / 3;
    maxHeight = (LINES - TOP_OFFSET) / 3;

    // Create ncurses windows
    window = newwin(maxHeight * 3, maxWidth * 3, TOP_OFFSET, LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH + 1);
    wrefresh(window);


    leftPanel = newwin(maxHeight * 3, LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH, TOP_OFFSET, 0);
    box(leftPanel, 0, 0);
    wrefresh(leftPanel);

    init_pair (GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair (RED, COLOR_RED, COLOR_BLACK);
}

void LevelEditor::createMap(){

    configureDimensions();
    createTowers();

    deleteWindows();
    if(saveMap){
        saveMapFile();
    }
    curs_set(0);
}

bool LevelEditor::editMap(const string & filename){

    if(! loadFile(filename)){
        deleteWindows();
        return false;
    }

    createTowers();

    deleteWindows();
    if(saveMap){
        saveMapFile();
    }
    curs_set(0);
    return true;
}

void LevelEditor::configureDimensions(){

    mvwprintw(leftPanel, 1, 1, "Dimensions setup.");
    mvwprintw(leftPanel, 3, 1, "Use arrows to set width");
    mvwprintw(leftPanel, 4, 1, "and height of the map.");
    mvwprintw(leftPanel, 6, 1, "Confirm dimensions by pressing enter.");
    wrefresh(leftPanel);

    int c = 0;
    width = MIN_WIDTH;
    height = MIN_HEIGHT;

    while(c != 10){  //key enter

        c = getch();

        switch(c){

            case KEY_UP:
                if(height - 1 < MIN_HEIGHT){
                    break;
                }
                height --;
                break;

            case KEY_LEFT:
                if(width - 1 < MIN_WIDTH){
                    break;
                }
                width --;
                break;

            case KEY_RIGHT:
                if(width + 1 > maxWidth){
                    break;
                }
                width ++;
                break;
            case KEY_DOWN:
                if(height + 1 > maxHeight){
                    break;
                }
                height ++;
                break;
            default:
                break;
        }
        clearWindow();
        renderBorderPreview();
        wrefresh(window);
    }
}

void LevelEditor::renderBorderPreview(){

    for (int i = 0; i < width; i++){
        renderTower(Coordinate(i, 0), 'W');
        renderTower(Coordinate(i, height - 1), 'W');
    }
    for (int j = 1; j < height - 1; j++){
        renderTower(Coordinate(0, j), 'W');
        renderTower(Coordinate(width - 1, j), 'W');
    }
}

void LevelEditor::renderPreview(){

    clearWindow();
    renderBorderPreview();

    for(auto it: towers){
        renderTower(it.first, it.second);
    }
    wrefresh(window);
}

void LevelEditor::clearWindow(){

    for(int i = 0; i < maxWidth * 3; i++){
        for(int k = 0; k < maxHeight * 3; k++){
            mvwaddch(window, k, i, ' ');
        }
    }
}

void LevelEditor::clearLeftWindow(){

    for(int i = 0; i < LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH; i++){
        for(int k = 0; k < maxHeight * 3; k++){
            mvwaddch(leftPanel, k, i, ' ');
        }
    }
}

void LevelEditor::renderTower(const Coordinate & pos, char type){

    int x = pos.getX();
    int y = pos.getY();

    switch (type)
    {
    case 'w':
    case 'W':
        mvwprintw(window, y * 3, x * 3,     "┌─┐");
        mvwprintw(window, y * 3 + 1, x * 3, "│█│");
        mvwprintw(window, y * 3 + 2, x * 3, "└─┘");
        break;
    case 's':
    case 'S':
        wattron (window, COLOR_PAIR( RED ));
        mvwprintw(window, y * 3, x * 3,     "╝ ╚");
        mvwprintw(window, y * 3 + 1, x * 3, " + ");
        mvwprintw(window, y * 3 + 2, x * 3, "╗ ╔");
        wattroff (window, COLOR_PAIR( RED ));
        break;
    case 'b':
    case 'B':
        wattron (window, COLOR_PAIR( GREEN ));
        mvwprintw(window, y * 3, x * 3,     "╔╦╗");
        mvwprintw(window, y * 3 + 1, x * 3, "╠╬╣");
        mvwprintw(window, y * 3 + 2, x * 3, "╚╩╝");
        wattroff (window, COLOR_PAIR( GREEN ));
        break;
    case 't':
    case 'T':
        mvwprintw(window, y * 3, x * 3,     "┌ ┐");

        mvwprintw(window, y * 3 + 2, x * 3, "└ ┘");
        break;
    default:
        break;
    }
}

void LevelEditor::createTowers(){

    clearLeftWindow();
    box(leftPanel, 0, 0);
    mvwprintw(leftPanel, 1, 1, "Build towers.");
    mvwprintw(leftPanel, 3, 1, "Use arrows to set location.");
    mvwprintw(leftPanel, 4, 1, "Press key on keyboard to add tower");
    mvwprintw(leftPanel, 5, 1, "to current cursor location.");
    mvwprintw(leftPanel, 7, 1, "W = wall");
    mvwprintw(leftPanel, 8, 1, "T = place where player can build");
    mvwprintw(leftPanel, 9, 1, "    towers in the game");
    mvwprintw(leftPanel, 10, 1, "S = spawn of enemies (Multiple");
    mvwprintw(leftPanel, 11, 1, "    spawns can be built in one map)");
    mvwprintw(leftPanel, 12, 1, "B = Player's base (target of enemies)");
    mvwprintw(leftPanel, 13, 1, "    only one can be built.");

    mvwprintw(leftPanel, 15, 1, "Delete = deletes object under cursor.");
    mvwprintw(leftPanel, 16, 1, "V      = validates if map can be saved.");
    mvwprintw(leftPanel, 17, 1, "Enter  = saves the map and exits level editor.");
    mvwprintw(leftPanel, 18, 1, "E      = exit editor without save.");
    wrefresh(leftPanel);

    int c = 0;
    int x = 1;
    int y = 1;

    wmove(window, y * 3 + 1, x * 3 + 1);
    curs_set(1);

    while(true){
        c = getch();
        int tmp;

        switch(c){
            case 10: //key enter - save map
                if(validateMap()){
                    werase(window);
                    mvwaddstr(window, 5, 5, "Save map");
                    mvwaddstr(window, 7, 5, "Y - yes, N - no (exit without save)");
                    mvwaddstr(window, 8, 5, "C - cancel (back to editor)");
                    wrefresh(window);
                    timeout(10000);
                    tmp = getch();
                    timeout(10);
                    if(tmp == 121){ // key y
                        return;
                    }
                    if(tmp == 110){
                        saveMap = false;
                        return;
                    }
                }
                break;
            case 101: // key e - exit
                werase(window);
                mvwaddstr(window, 5, 5, "Exit without save?");
                mvwaddstr(window, 7, 5, "Y - yes, N - no (go back to editor)");
                wrefresh(window);
                timeout(10000);
                tmp = getch();
                timeout(10);

                if(tmp == 121){ // key y
                    saveMap = false;
                    return;
                }
                break;
            case KEY_RIGHT:
                if(x + 2 < width){
                    x++;
                }break;
            case KEY_LEFT:
                if(x > 1){
                    x--;
                }break;
            case KEY_UP:
                if(y > 1){
                    y--;
                }break;
            case KEY_DOWN:
                if(y + 2 < height){
                    y++;
                }break;
            case 118: // key v
                validateMap();
                break;
            case 119: // key w
                towers[Coordinate(x, y)] = 'W';
                break;
            case 116: // key t
                towers[Coordinate(x, y)] = 'T';
                break;
            case 115: // key s
                towers[Coordinate(x, y)] = 'S';
                break;
            case 98: // key b
                towers[Coordinate(x, y)] = 'B';
                break;
            case KEY_DC: //key delete
            case KEY_BACKSPACE:
                towers.erase(Coordinate(x, y));
                break;
            default:
                break;
        }
        renderPreview();
        wmove(window, y * 3 + 1, x * 3 + 1);
        wrefresh(window);
    }
}

bool LevelEditor::validateMap(){

    int baseCount = 0;
    int spawnCount = 0;
    int towerPlaceCount = 0;
    bool status = false;

    // Iterate through object map and check how many required items are set up.
    for(auto it: towers){
        switch(it.second){
            case 'T':
                towerPlaceCount++;
                break;
            case 'S':
                spawnCount++;
                break;
            case 'B':
                baseCount++;
                break;
            default:
                break;
        }
    }

    clearWindow();
    renderBorderPreview();

    if(baseCount == 0){
        mvwprintw(window, 5, 5, "You need to add player's base.");
        mvwprintw(window, 7, 5, "Everyone needs place to sleep.");
    }
    else if(baseCount > 1){
        mvwprintw(window, 5, 5, "Player can have just one base.");
        mvwprintw(window, 7, 5, "Why you want to protect two homes,");
        mvwprintw(window, 8, 5, "when you can just protect one...");
    }
    else if(spawnCount == 0){
        mvwprintw(window, 5, 5, "Add at least one enemy spawn.");
        mvwprintw(window, 7, 5, "Enemies have no place to spawn :/");
    }
    else if(towerPlaceCount == 0){
        mvwprintw(window, 5, 5, "Add at least one tower place.");
        mvwprintw(window, 7, 5, "It's to easy for you?");
        mvwprintw(window, 8, 5, "Did you tried to play it?");
    }
    else{
        status = true;
        mvwprintw(window, 5, 5, "Well done, map is valid.");
    }

    mvwprintw(window, 10, 5, "Press any key to continue.");

    wrefresh(window);

    // Wait 10s until player reads messages or he presses a key
    timeout(10000);
    getch();
    timeout(10);

    return status;
}

void LevelEditor::saveMapFile(){

    // Find first available filename for the map
    string filename = string(MAPS_DIR) + "/map";
    int suffix = 1;
    stringstream tmp;

    while(true){
        ifstream inStream;
        tmp = stringstream("");
        tmp << filename << suffix << ".map";
        inStream = ifstream(tmp.str());
        if(!inStream){
            break;
        }
        inStream.close();
        suffix ++;
    }

    // Create ouput filestream
    ofstream outputFile;
    outputFile.open(tmp.str());

    // Create data stringstream  (It will be written to filestream after hash is in place)
    stringstream outputStr;

    // Write out data
    outputStr << width << " " << height << endl;

    for(auto it: towers){
        outputStr << it.second << " " << it.first.getX() << " " << it.first.getY() << endl;
    }

    // Generate checksum for map data, to prevent manualuser editing.
    const string tmps = outputStr.str();
    outputStr << ";" << generateChecksum(tmps);

    // Write map to output file
    outputFile << outputStr.str();
    outputFile.close();

    // Render success message
    stringstream banner;
    banner << "Map has been saved to: map" << suffix << ".map";
    mvprintw(LINES / 2, COLS / 2 - banner.str().size() / 2, banner.str().c_str());
    refresh();
    timeout(5000);
    getch();
    timeout(DEFAULT_TIMEOUT);
}

unsigned LevelEditor::generateChecksum(const string & str){

    unsigned res = 1;

    for(char c: str){
        res = (res * 7) + (((unsigned) c) + 1);
    }

    return res;
}

bool LevelEditor::checkChecksum(const string & filename){

    // Open file.
    ifstream inStream(filename);
    if(!inStream || !inStream.is_open()){
        return false;
    }

    unsigned res = 1;
    char tmp;
    // Load chars from file until ';'
    while((inStream >> noskipws >> tmp) && tmp != ';'){
        if(inStream.eof()){
            inStream.close();
            return false;
        }
        res = (res * 7) + (((unsigned) tmp) + 1);
    }

    unsigned cmp;
    inStream >> skipws >> cmp;
    if(cmp == res){
        inStream.close();
        return true;
    }

    // Checksums doesnt match
    erase ();
    refresh ();
    endwin ();

    inStream.close();
    return false;
}

void LevelEditor::deleteWindows(){
    clearLeftWindow();
    wrefresh(leftPanel);
    delwin(leftPanel);

    clearWindow();
    wrefresh(window);
    delwin(window);
}

bool LevelEditor::loadFile(const string & filename){

    // Check if map has a valid checksum
    if(! checkChecksum(filename)){
        return false;
    }

    // Load file
    ifstream inStream;
    inStream = ifstream(filename);
    if(!inStream || ! inStream.is_open()){
        return false;
    }

    // Load dimensions
    inStream >> width >> height;

    // Check screen size
    if((width * 3) + LEFT_PANEL_WIDTH + RIGHT_PANEL_WIDTH - COLS > 0 || (height * 3) + 1 - LINES > 0){
        inStream.close();
        return false;
    }

    // Load towers
    char type;
    int x, y;

    while(inStream >> type >> x >> y){
        towers[Coordinate(x, y)] = type;
    }

    return true;
}
