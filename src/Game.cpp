#include "Game.hpp"

using namespace std;

#define COLOR 1


Game::Game() {

}

Game::~Game(){
}

void Game::run(){

    if( ! prepareWindow() ){
        return;
    }

    mainMenuHandler();

    closeWindow();
}

bool Game::loadMap(const string & filePath, const string & fileName, int difficulty){

    this->gameField = GameField(fileName, difficulty);
    if(this->gameField.loadBoard(filePath)){
        return true;
    }
    return false;
}

bool Game::loadSave(const string & saveName){

    this->gameField = GameField();
    if(this->gameField.loadSave(saveName)){
        return true;
    }
    return false;
}

void Game::play(){

    int c;
    init_pair (COLOR, COLOR_YELLOW, COLOR_BLACK);
    timeout(DEFAULT_TIMEOUT);

    char end_status = '0';

    // Main game loop
    while(end_status == '0'){

        // Wait for input from player
        c=getch();

        switch(c){
            case 27: // keq Esc to quit the game
            case 113: // key q to quit the game
                end_status = 'q';
                break;
            case 115: // key s to save the game
                end_status = 's';
                erase ();
                refresh ();
                endwin ();
                gameField.saveGame();
                break;
            case 98: // key b to enter tower build mode
                towerBuildMode();
            default:
                break;
        }

        gameField.increaseReloads();

        gameField.moveEnemies();

        gameField.spawnEnemy();

        gameField.towersShoot();

        gameField.checkEnemies();

        if(gameField.getPlayer().isDead()){
            end_status = 'd';
        }

        gameField.render(c);

    }
    int score = gameField.getPlayer().getScore();
    string mapName = gameField.getMapName();
    string saveName = gameField.getSaveName();
    int difficulty = gameField.getDifficulty();

    gameField.destruct();

    if(end_status == 's'){
        gameSavedSceneHandler(saveName);
    }
    else if(score > 0){
        scoreSceneHandler(score, mapName, difficulty);
    }
    return;
}

void Game::towerBuildMode(){

    gameField.setBuildMode();

    int x = 1;
    int y = 1;
    int c;

    // Move cursor to upper left corner
    wmove(gameField.getWindow(), y * 3 + 1, x * 3 + 1);
    curs_set(2);

    // Shop game loop
    while(true){

        // Wait for input from player
        c = getch();

        switch(c){
            case 100: // key 'd'
            case KEY_RIGHT:
                if(x + 2 < gameField.getWidth()){
                    x++;
                }break;
            case 97: // key 'a'
            case KEY_LEFT:
                if(x > 1){
                    x--;
                }break;
            case 119: // key 'w'
            case KEY_UP:
                if(y > 1){
                    y--;
                }break;
            case 115: // key 's'
            case KEY_DOWN:
                if(y + 2 < gameField.getHeight()){
                    y++;
                }break;
            case 113: // 'q' to quit build mode
                gameField.setPlayMode();
                gameField.setCursorOn(NULL);
                curs_set(0);
                return;
            case 49: // key '1' to buy Destroyer
                gameField.buyTower(Coordinate(x, y), 'D');
                break;
            case 50: // key '2' to buy Sniper
                gameField.buyTower(Coordinate(x, y), 'S');
                break;
            case 117: // key u to upgradeTower
                gameField.upgradeSelectedTower();
                break;
            default:
                break;
        }

        // When active tower is under cursor, set game to upgrade mode
        if( gameField.getTowerAt(Coordinate(x, y)) != NULL && gameField.getTowerAt(Coordinate(x, y))->getDemage() > 0 ){
            gameField.setUpgradeMode();
            gameField.setCursorOn(gameField.getTowerAt(Coordinate(x, y)));
        }
        else{
            gameField.setBuildMode();
            gameField.setCursorOn(NULL);
        }

        gameField.render(c);
        wmove(gameField.getWindow(), y * 3 + 1, x * 3 + 1);
        wrefresh(gameField.getWindow());
    }
}

bool Game::prepareWindow(){

    // Set up ncurses
    setlocale(LC_ALL, ""); // It allows UNICODE chars

    initscr ();
    timeout(DEFAULT_TIMEOUT); // "fps like - lower faster"
    cbreak ();
    noecho ();
    start_color ();
    keypad (stdscr, TRUE);
    curs_set(0);

    if (!has_colors ()) {
        endwin ();
        fputs ("Colors dont work", stderr);
        return false;
    }

    init_pair (COLOR, COLOR_YELLOW, COLOR_BLACK);


    attron (COLOR_PAIR( COLOR ));
    attron (A_BOLD);
    mvaddstr (0, COLS / 2 - 11, "GAME TITLE WILL BE HERE");
    attroff (COLOR_PAIR( COLOR ));
    refresh();
    return true;
}

void Game::closeWindow(){

    // Close ncurses
    erase ();
    refresh ();
    endwin ();
}

void Game::mainMenuHandler(){

    vector<string> mainMenuChoices = {
        "New Game", "Load Save", "Create a new map",
        "Edit map", "Rankings", "Help", "Exit"
    };

    while(true){
        GameMenu mainMenu (mainMenuChoices, "Main Menu");
        int choice = mainMenu.getChoice();

        switch(choice){
            case 0:
                mapMenuHandler();
                break;
            case 1:
                savesMenuHandler();
                break;
            case 2:
                createMap();
                break;
            case 3:
                editMapMenuHandler();
                break;
            case 4:
                rankingsMapChoiceHandler();
                break;
            case 5:
                displayHelp();
                break;
            default:
                return;
        }
    }
}

void Game::mapMenuHandler(){

    // Get player made maps
    vector<string> maps = getFileChoices(MAPS_DIR, "map", "map");
    int playerMaps = maps.size();

    // Add example maps
    maps.push_back("example_large.map");
    maps.push_back("example_regular.map");
    maps.push_back("example_small.map");

    // Get player choice
    GameMenu mapsMenu (maps, "Please select a map");
    int choice = mapsMenu.getChoice();

    if(choice == 0){
        return;
    }

    // Get map location
    string mapPath = maps[choice];
    stringstream filename;

    if(choice < playerMaps){
        filename << MAPS_DIR << "/" << mapPath;
    }
    else{
        filename << EXAMPLE_MAPS_DIR << "/"  << mapPath;
    }
    mapPath = filename.str();

    // Get difficulty
    int difficulty = difficultyMenuHandler();

    // Load the game
    bool loadStatus = loadMap(mapPath, maps[choice], difficulty);

    if(loadStatus){
        play();
    }else{
        loadFailedHandler();
    }
    return;
}

void Game::savesMenuHandler(){

    vector<string> saves = getFileChoices(SAVES_DIR, "save", "save");
    GameMenu savesMenu (saves, "Please select a save");
    int choice = savesMenu.getChoice();

    if(choice == 0){
        return;
    }

    string saveName = saves[choice];

    stringstream filename;
    filename << SAVES_DIR << "/"  << saveName;
    saveName = filename.str();

    bool loadStatus;

    loadStatus = loadSave(saveName);

    if(loadStatus){
        play();
    }else{
        loadFailedHandler();
    }
    return;
}

void Game::scoreSceneHandler(int score, string filename, int difficulty){

    vector<string> choices = {"%input%", "Save score", "Discard"};
    stringstream tmp;
    tmp << "Your score at map " << filename << " is: " << score << ". Please enter your name.";
    GameMenu saveMenu(choices, tmp.str());

    string playerName;
    int choice = saveMenu.getChoice(playerName);

    // Player clicked on discard
    if (choice == 2){
        return;
    }

    // Check if player name is not empty
    if(playerName == ""){
        playerName = "Unknown";
    }

    // Add difficulty after player name
    switch(difficulty){
        case 0:
            playerName = playerName + "(Easy)";
            break;
        case 1:
            playerName = playerName + "(Normal)";
            break;
        case 2:
            playerName = playerName + "(Hard)";
            break;
        case 3:
        default:
            playerName = playerName + "(Hardcore)";
            break;
    }


    string scoreFilePath =  string(SAVES_DIR) + "/score.score";
    ifstream inStream(scoreFilePath);
    stringstream results;

    // Check if score file exists and is valid.
    if(!inStream || !LevelEditor::checkChecksum(scoreFilePath)){
        results << filename << " " << playerName << " " << score << endl;
    }
    // Copy previous results if is valid
    else{
        char tmp;
        while((inStream >> noskipws >> tmp) && tmp != ';'){
            results << tmp;
        }
        results << filename << " " << playerName << " " << score << endl;
    }

    // Create new checksum
    const string tmpS = results.str();
    results << ";" << LevelEditor::generateChecksum(tmpS);

    // Write scores to file
    ofstream outputFile(scoreFilePath);
    outputFile << results.str();
    outputFile.close();
}

void Game::gameSavedSceneHandler(string saveName){

    vector<string> choices = {"OK"};

    stringstream message;
    message << "Your game was saved to save file: " << saveName;
    GameMenu menu (choices, message.str());
    menu.getChoice();
}

void Game::rankingsMapChoiceHandler(){

    vector<string> maps = getFileChoices(MAPS_DIR, "map", "map");

    // Add example maps
    maps.push_back("example_large.map");
    maps.push_back("example_regular.map");
    maps.push_back("example_small.map");

    GameMenu mapsMenu (maps, "Please select a map to view a ranking.");
    int choice = mapsMenu.getChoice();

    if(choice == 0){
        return;
    }

    rankingsDisplayHandler(maps[choice]);
}

void Game::rankingsDisplayHandler(string mapName){

    stringstream message;
    message << "Best scores on map: " << mapName;

    vector<string> scores = getScores(mapName);
    GameMenu menu (scores, message.str());
    menu.getChoice();
}

void Game::loadFailedHandler(){

    vector<string> choices = {
        "File loading has failed",
        "Either your screen is too small",
        "or file has been edited",
        "",
        "Press Enter to continue"
    };
    GameMenu menu(choices, "Load failed");
    menu.getChoice();
}

int Game::difficultyMenuHandler(){

    vector<string> choices = {"Easy", "Normal", "Hard", "Hardcore"};

    // Get player choice
    GameMenu mapsMenu (choices, "Please choose a difficulty");
    int choice = mapsMenu.getChoice();

    return choice;
}

void Game::editMapMenuHandler(){

    // Get player made maps
    vector<string> maps = getFileChoices(MAPS_DIR, "map", "map");
    int playerMaps = maps.size();

    // Add example maps
    maps.push_back("example_large.map");
    maps.push_back("example_regular.map");
    maps.push_back("example_small.map");

    // Get player choice
    GameMenu mapsMenu (maps, "Please select a map to edit");
    int choice = mapsMenu.getChoice();

    if(choice == 0){
        return;
    }

    // Get map location
    string mapPath = maps[choice];
    stringstream filename;

    if(choice < playerMaps){
        filename << MAPS_DIR << "/" << mapPath;
    }
    else{
        filename << EXAMPLE_MAPS_DIR << "/"  << mapPath;
    }

    // Edit map and check if was succesfully loaded.
    LevelEditor editor;;
    if(!editor.editMap(filename.str())){
        loadFailedHandler();
    }
}

vector<string> Game::getFileChoices(string filePath, string namePrefix, string suffix){

    vector<string> files = {"Back"};

    int positionSuffix = 1;

    // Add all available maps to the vector
    while(true){

        ifstream inStream;
        stringstream tmp;
        tmp << filePath << "/" << namePrefix << positionSuffix << "." << suffix;
        inStream = ifstream(tmp.str());

        // Check if file can be opened
        if(!inStream){
            break;
        }
        inStream.close();

        tmp.str("");
        tmp << namePrefix << positionSuffix << "." << suffix;
        files.push_back(tmp.str());

        positionSuffix ++;
    }

    return files;
}

bool Game::compareResults(pair<int, string> & a, pair<int, string> & b){
    return a.first > b.first;
}

vector<string> Game::getScores(string mapName){

    string scoreFilePath = string(SAVES_DIR) + "/score.score";
    ifstream inStream(scoreFilePath);

    // Check if score file exists and is valid.
    if(!inStream || !LevelEditor::checkChecksum(scoreFilePath)){
        return {"None results yet."};
    }

    // Copy previous results if is valid
    else{
        vector<pair<int, string>> results;

        string map, playerName;
        int score;

        // Load all results
        while(true){
            inStream >> map;

            if(map[0] == ';'){
                break;
            }
            inStream >> playerName >> score;
            if(mapName == map){
                results.push_back(make_pair(score, playerName));
            }
        }

        if(results.size() == 0){
            return {"None results yet."};
        }

        // Sort results.
        sort(results.begin(), results.end(), compareResults);

        // Fill output vector
        vector<string> out;
        for(pair<int, string> result: results){
            stringstream tmp;
            tmp << result.second << ": " << result.first;
            out.push_back(tmp.str());
        }

        return out;
    }
}

void Game::createMap(){

    LevelEditor editor;
    editor.createMap();
}

void Game::displayHelp(){

    vector<string> helpChoices = {"Back", "Game goal", "Enemies", "Towers"};

    while(true){
        GameMenu mainMenu (helpChoices, "Choose help section");
        int choice = mainMenu.getChoice();

        switch(choice){
            case 0:
                return;
            default:
                displayHelpPage(choice);
                break;
        }
    }

}

void Game::displayHelpPage(int page){

    WINDOW * window = newwin(LINES - 4, COLS, 2, 0);
    box(window, 0, 0);
    timeout(1000000);

    switch(page){
        case 1:
            mvwprintw(window, 2, 15, "Game goal");

            mvwprintw(window, 5, 1, "Goal of the game is to protect players base, as long as it's possible.");
            mvwprintw(window, 7, 1, "Enemies are beeing spawned at spawns. After spawn they pathfind");
            mvwprintw(window, 8, 1, "to players base using the shortest route possible");
            mvwprintw(window, 9, 1, "(on empty blocks without towers/ walls).");
            mvwprintw(window, 10, 1, "If enemy reaches players base, 1 healt point is substracted.");
            mvwprintw(window, 11, 1, "When player has 0 health game ends.");
            mvwprintw(window, 12, 1, "Enemies are getting better as game progresses.");

            mvwprintw(window, 14, 1, "Player's ╔╦╗");
            mvwprintw(window, 15, 1, "base:    ╠╬╣");
            mvwprintw(window, 16, 1, "         ╚╩╝");

            mvwprintw(window, 14, 14, "Enmey  ╝ ╚");
            mvwprintw(window, 15, 14, "spawn:  + ");
            mvwprintw(window, 16, 14, "       ╗ ╔");

            mvwprintw(window, 14, 25, "Wall: ┌─┐");
            mvwprintw(window, 15, 25, "      │█│");
            mvwprintw(window, 16, 25, "      └─┘");

            mvwprintw(window, 14, 35, "Place  ┌ ┐");
            mvwprintw(window, 15, 35, "for a     ");
            mvwprintw(window, 16, 35, "tower: └ ┘");
            break;

        case 2:
            mvwprintw(window, 2, 15, "Enemy types");

            mvwprintw(window, 5, 1, "Puncher");
            mvwprintw(window, 6, 1, "███   mid health - Nothing special about him :]");
            mvwprintw(window, 7, 1, " o    mid speed");
            mvwprintw(window, 8, 1, "      reward: %d$", PUNCHER_REWARD);

            mvwprintw(window, 10, 1, "Sneaker");
            mvwprintw(window, 11, 1, "███   low health - He tries to find path to the player's base");
            mvwprintw(window, 12, 1, " s    mid speed    outside the reach of towers. Be aware of him!");
            mvwprintw(window, 13, 1, "      reward: %d$", SNEKAER_REWARD);

            mvwprintw(window, 15, 1,  "Tank");
            mvwprintw(window, 16, 1,  "███   high health - Boss in late game. When is beeing hitted,");
            mvwprintw(window, 17, 1, "\\+/   slow speed    spawns other enemies on his position.");
            mvwprintw(window, 18, 1,  " V    reward: %d$", TANK_REWARD);
            break;

        case 3:
            mvwprintw(window, 1, 15, "Towers");

            mvwprintw(window, 4, 1, "Player can enter build mode and from reward money");
            mvwprintw(window, 5, 1, "buy new defense towers or upgrade existing ones.");
            mvwprintw(window, 6, 1, "Towers can be built only on tower places: ┌ ┐");
            mvwprintw(window, 8, 1, "                                          └ ┘");

            mvwprintw(window, 9, 1,  "Sniper ┌|┐ high demage");
            mvwprintw(window, 10, 1, "       │S│ long range");
            mvwprintw(window, 11, 1, "       └─┘ slow reload");
            mvwprintw(window, 11, 1, "%d $", SNIPER_PRICE);

            mvwprintw(window, 9, 26,  "Destroyer ┌|┐ low demage");
            mvwprintw(window, 10, 26, "          │D│ mid range");
            mvwprintw(window, 11, 26, "          └─┘ mid reload");
            mvwprintw(window, 11, 26, "%d $", DESTROYER_PRICE);
            break;

        default:
            break;
    }

    wrefresh(window);

    // Wait until player presses a key
    getch();

    // Erase help text
    timeout(DEFAULT_TIMEOUT);
    werase(window);
    wrefresh(window);
    delwin(window);
}
