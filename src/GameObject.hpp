#pragma once

#include <cstdlib>
#include <ncurses.h>
#include <iostream>
#include <sstream>

#include "Config.hpp"
#include "Coordinate.hpp"

using namespace std;

#define GREEN 2
#define BLUE 3
#define RED 4
#define CYAN 5
#define MAGENTA 6
#define YELLOW 7

/** Virtual class, which every object which is rendered in game window inherits */
class GameObject{

    public:

        GameObject (Coordinate pos, unsigned objectId);

        virtual ~GameObject() = default;

        Coordinate getPos() const;

        void setPos(Coordinate pos);

        /**
         * Renders object on the ncurses window.
         * @param window - ncurses WINDOW * window where object will be rendered
        */
        virtual void render(WINDOW * window) const = 0;

        /**
         * Outputs object as a string.
         * It is used when game progress is beeing saved to file.
         * @returns string representation of the object.
        */
        virtual string toString() = 0;

    protected:

        Coordinate pos;

        unsigned objectId;
};
/*   TOWERS       SPACE FOR TOWER      ENEMAK       BASE        SPAWN
        ┌─┐            ┌ ┐                ^         ╔╦╗         ╝ ╚
        │x═                              <*>        ╠╬╣          +
        └─┘            └ ┘                v         ╚╩╝         ╗ ╔

        ┌║┐            ┌─┐
        │o│            │█│
        └─┘            └─┘

        ┌─┐
        │o│
        ╱─┘

*/
