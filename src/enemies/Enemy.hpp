#pragma once

#include <queue>
#include <deque>
#include <map>
#include <algorithm>

#include "../GameObject.hpp"


// Forward declaration to prevent circular dependency
class GameField;

using namespace std;

/**
 * Virtual class, that is inherited by all enemies.
*/
class Enemy : public GameObject{

    public:

        /**
         * Constructor
         * @param x - x coordinate of the tower
         * @param y - y coordinate of the tower
         * @param objectId - id under which object will be saved to save file
         * @param speed - how fast is enemy moving (lower ~ faster)
         * @param maxhealth
         * @param reward - how much money player will get when he enemy is killed
         */
        Enemy(Coordinate pos, unsigned objectId, double speed, double maxHealth, double reward);

        ~Enemy();

        /**
         * Renders enemy in the ncurses window.
         * @param window - ncurses WINDOW * window where enemy will be rendered
        */
        virtual void render(WINDOW * window) const = 0;

        /**
         * Generates path which will enemy follow.
         * @param gameField instance of GameField, it is used to get tower layout.
        */
        virtual void generatePath(GameField * gameField) = 0;

        /**
         * Adds one point to enemy move cooldown.
        */
        void increaseMoveReload();

        /**
         * When currentMoveReload reaches moveCooldown,
         * enemy moves to the next point of the path
         * and currentMoveReload is reset to 0
        */
        void move();

        /**
         * Substracts health from enemy
         * @param dmg how much health is substracted from health.
         * @param gameField pointer to gamefield, so tank can spawn childs when
         * health crosses some margins.
        */
        virtual void dealDemage(double dmg, GameField * gameField);

        double getHealth() const;

        bool isPathEmpty() const;

        double getReward() const;

        /**
         * Returns enemy representation in a string format.
         * It is used when saving object to save file.
        */
        virtual string toString() final;

        void setCurrentMoveCooldown(double cooldown);

        void setHealth(double health);

    protected:

        double health = 0; /**< Current health of the enemy */
        double maxHealth = 0; /**< Max health of the enemy */

        double healing = 0; /**< How fast enemy heals */ // TODO not implemented yet

        double moveCooldown; /**< How fast can enemy move (lower ~ faster) */
        double currentMoveCooldown = 0; /**< if >= moveCooldown, enemy can move */

        double reward; /**< How much money will player get, when enemy is killed */

        deque<Coordinate> path; /**< coordinates of precomputed path */

        /**
         * Renders healthbar above the enemy
         * @param window - ncurses window, where healthbar will be rendered
        */
        void renderHealthbar(WINDOW * window) const;

        /**
         * Generates new path from enemy to base using BFS algorithm.
         * @param gameField - instance of GameField from which tower layout is getted
         * @param start - starting coordinate of pathfinding
        */
        void bfs(GameField * gameField, Coordinate start);

        /**
         * Pathfinding algorithms saves path by map of ancestors.
         * methods extracts path and saves it to the enemy path queue.
         * @param pos node from which path is backtraced
         * @param parents - map of node ancestors
        */
        void tracePath(Coordinate pos, map<Coordinate,Coordinate> & parents);

        /**
         * Struct represents edge in graph when using Jarnik's agorithm
        */
        typedef struct{
            Coordinate from;
            Coordinate to;
            double rating;
        } SEdge;

        /**
         * Generates new path from enemy to base using jarniks algorithm.
         * @param gameField - instance of GameField from which tower rating map and tower layout is getted.
         * @param start - starting coordinate of pathfinding
        */
        void jarnik(GameField * gameField, Coordinate start);

        /**
         * Checks if position is suitable for enemy to move on.
         * @param gameField - instance of GameField from which tower layout is getted
         * @param pos - position which is beeing checked.
        */
        bool checkCoords(GameField * gameField, Coordinate pos);

        /**
         * Compare two SEdge graph edges
         * It is used in jarnik's algorhitm to find edge with the lowest rating
         * @returns true if edge1.rating < edge2.rating
        */
        static bool compareEdges ( SEdge & edge1 , SEdge & edge2 );

};
