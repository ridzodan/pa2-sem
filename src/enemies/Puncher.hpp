#pragma once

#include <queue>

#include "Enemy.hpp"

using namespace std;


class Puncher : public Enemy{

    public:

        Puncher(Coordinate pos, double speed, double maxHealth, double reward);

        ~Puncher();

        /**
         * Renders puncher in the ncurses window.
         * @param window - ncurses WINDOW * window where puncher will be rendered
        */
        void render(WINDOW * window) const final;

        /**
         * Generates path which will enemy follow.
         * Puncher uses BFS pathfinding.
         * @param gameField instance of GameField, it is used to get tower layout.
        */
        void generatePath(GameField * gameField) final;

    private:

};
