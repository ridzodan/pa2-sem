#include "Sneaker.hpp"
#include "../GameField.hpp"

using namespace std;


Sneaker::Sneaker (Coordinate pos, double speed, double maxHealth, double reward) : \
    Enemy(pos, SNEAKER_ID, speed, maxHealth, reward) {

}

Sneaker::~Sneaker(){

}

void Sneaker::render(WINDOW * window) const{
    mvwprintw(window, pos.getY() * 3 + 1, pos.getX() * 3 + 1, "s");

    renderHealthbar(window);
}

void Sneaker::generatePath(GameField * gameField){

    // Delete previously generated path
    path = deque<Coordinate>();
    jarnik(gameField, pos);
}
