#pragma once

#include <queue>

#include "Enemy.hpp"

using namespace std;


class Tank : public Enemy{

    public:

        Tank(Coordinate pos, double speed, double maxHealth, double reward);

        ~Tank();

        /**
         * Renders puncher in the ncurses window.
         * @param window - ncurses WINDOW * window where puncher will be rendered
        */
        void render(WINDOW * window) const final;

        /**
         * Generates path which will enemy follow.
         * Puncher uses BFS pathfinding.
         * @param gameField instance of GameField, it is used to get tower layout.
        */
        void generatePath(GameField * gameField) final;

        /**
         * Substracts health from a tank
         * If health crosses specified margins extra enemies are spawned at the tank.
         * @param dmg how much health is substracted from health.
         * @param gameField pointer to gamefield, so tank can spawn childs when
         * health crosses some margins.
        */
        virtual void dealDemage(double dmg, GameField * gameField) final;

    private:

};
