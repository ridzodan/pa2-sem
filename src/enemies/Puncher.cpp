#include "Puncher.hpp"

using namespace std;


Puncher::Puncher (Coordinate pos, double speed, double maxHealth, double reward) : \
    Enemy(pos, PUNCHER_ID, speed, maxHealth, reward) {

}

Puncher::~Puncher(){

}

void Puncher::render(WINDOW * window) const{
    mvwprintw(window, pos.getY() * 3 + 1, pos.getX() * 3 + 1, "o");

    renderHealthbar(window);
}

void Puncher::generatePath(GameField * gameField){

    // Delete previously generated path
    path = deque<Coordinate>();
    bfs(gameField, pos);
}
