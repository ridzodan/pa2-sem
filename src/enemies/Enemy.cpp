#include "Enemy.hpp"
#include "../GameField.hpp"

using namespace std;


Enemy::Enemy (Coordinate pos, unsigned objectId, double speed, double maxHealth, double reward)
    : GameObject(pos, objectId), health(maxHealth), maxHealth(maxHealth), moveCooldown(speed), \
    reward(reward){

}

Enemy::~Enemy(){

}

void Enemy::increaseMoveReload(){
    currentMoveCooldown ++;
}

void Enemy::move(){

    if( currentMoveCooldown < moveCooldown || path.empty()){
        return;
    }
    currentMoveCooldown = 0;
    pos = path.front();
    path.pop_front();
}

void Enemy::dealDemage(double dmg, GameField * ){
    health = health - dmg;
}

double Enemy::getHealth() const{
    return health;
}

bool Enemy::isPathEmpty() const{
    return path.empty();
}

double Enemy::getReward() const{
    return reward;
}

void Enemy::renderHealthbar(WINDOW * window) const{

    int x = pos.getX();
    int y = pos.getY();

    // Choose color which will be healthbar rendered in
    if(health > (maxHealth * 2 / 3.0) ){
        wattron(window, COLOR_PAIR( GREEN ));
    }
    else if(health > (maxHealth / 3.0)){
        wattron(window, COLOR_PAIR( YELLOW ));
    }
    else{
        wattron(window, COLOR_PAIR( RED ));
    }

    // Render first char of a healthbar
    if(health <= (maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3, "*");
    }
    else if(health > (2 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3, "█");
    }
    else{
        mvwprintw(window, y * 3, x * 3, "#");
    }

    // Render second char of a healthbar
    if(health > (5 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 1, "█");
    }
    else if(health > (4 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 1, "#");
    }
    else if(health >= (3 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 1, "*");
    }

    // Render third char of a healthbar
    if(health > (8 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 2, "█");
    }
    else if(health > (7 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 2, "#");
    }
    else if(health >= (6 * maxHealth / 9.0)){
        mvwprintw(window, y * 3, x * 3 + 2, "*");
    }

    // Turn off color
    if(health > (maxHealth * 2 / 3.0) ){
        wattroff(window, COLOR_PAIR( GREEN ));
    }
    else if(health > (maxHealth / 3.0)){
        wattroff(window, COLOR_PAIR( YELLOW ));
    }
    else{
        wattroff(window, COLOR_PAIR( RED ));
    }

}

string Enemy::toString(){

    stringstream res;

    res << " " << objectId << " ";

    res << pos.getX() << " " << pos.getY() << " ";
    res << health << " " << maxHealth << " " << healing << " ";
    res << moveCooldown << " " << currentMoveCooldown << " " << reward;

    return res.str();
}

void Enemy::setCurrentMoveCooldown(double cooldown){
    currentMoveCooldown = cooldown;
}

void Enemy::setHealth(double health){
    this->health = health;
}

void Enemy::bfs(GameField * gameField, Coordinate start){

    queue<Coordinate> toVisit;
    toVisit.push(start);

    map<Coordinate, Coordinate> parents;
    parents[start] = start;

    Coordinate directions[] = {Coordinate(0, 1), Coordinate(0, -1), Coordinate(1, 0), Coordinate(-1, 0)};

    // Main bfs cycle
    while(!toVisit.empty()){
        Coordinate tmp = toVisit.front();
        toVisit.pop();

        // Check if destination has been reached.
        if(tmp == gameField->getBase()->getPos()){
            tracePath(tmp, parents);
            return;
        }

        // Check if current pos is empty (no towers under)
        if((tmp != start) && gameField->getTowerAt(tmp) != NULL){
            continue;
        }

        int randCoeff = rand();
        for(int i = 0; i < 4; i++){

            // Make it a little bit more random
            int k = (i + randCoeff) % 4;

            Coordinate next = tmp + directions[k];
            if(parents.find(next) == parents.end()){
                parents[next] = tmp;
                toVisit.push(next);
            }
        }

    }
    return;
}

void Enemy::tracePath(Coordinate pos, map<Coordinate, Coordinate> & parents){
    while(pos != parents[pos]){
        path.push_front(pos);
        pos = parents[pos];
    }
    return;
}

void Enemy::jarnik(GameField * gameField, Coordinate start){

    Coordinate directions[] = {Coordinate(0, 1), Coordinate(0, -1), Coordinate(1, 0), Coordinate(-1, 0)};

    map<Coordinate, Coordinate> parents;
    parents[start] = start;

    vector<SEdge> neighbours;

    // Add start neighbours
    for(int i = 0; i < 4; i++){
        if(checkCoords(gameField, start + directions[i])){
            SEdge tmp;
            tmp.from = start;
            tmp.to = start + directions[i];
            tmp.rating = gameField->getRatingAt(tmp.to);
            neighbours.push_back(tmp);
        }
    }

    // Sort edges
    sort(neighbours.begin(), neighbours.end(), compareEdges);

    while(neighbours.size() > 0){

        // Get edge with the lowest rating
        SEdge edge = neighbours[0];
        neighbours.erase(neighbours.begin());
        parents[edge.to] = edge.from;

        // Check if new node is players base
        if(edge.to == gameField->getBase()->getPos()){
            tracePath(edge.to, parents);
            return;
        }

        // Remove edges which are leading to the current node
        vector<SEdge>::iterator it = neighbours.begin();
        while (it != neighbours.end())
        {
            if(it->to == edge.to){
                it = neighbours.erase(it);
            }
            else{
                it++;
            }
        }

        // Add edges from visited node
        for(int i = 0; i < 4; i++){
            Coordinate tmpCoord = edge.to + directions[i];
            if(checkCoords(gameField, tmpCoord) && \
                parents.find(tmpCoord) == parents.end()){
                SEdge tmp;
                tmp.from = edge.to;
                tmp.to = tmpCoord;
                tmp.rating = gameField->getRatingAt(tmpCoord) + edge.rating;
                neighbours.push_back(tmp);
            }
        }

        // Sort edges
        sort(neighbours.begin(), neighbours.end(), compareEdges);
    }
}

bool Enemy::checkCoords(GameField * gameField, Coordinate pos){
    if(pos.getX() >= gameField->getWidth() || pos.getX() < 0 || pos.getY() < 0 || pos.getY() >= gameField->getHeight()){
        return false;
    }
    else if(pos == gameField->getBase()->getPos()){
        return true;
    }
    return (gameField->getTowerAt(pos) == NULL);
}

bool Enemy::compareEdges ( Enemy::SEdge & edge1 , Enemy::SEdge & edge2 ) {
	return ( edge1.rating < edge2.rating );
}
