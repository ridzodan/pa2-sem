#pragma once

#include "Enemy.hpp"

using namespace std;


class Sneaker : public Enemy{

    public:

        Sneaker(Coordinate pos, double speed, double maxHealth, double reward);

        ~Sneaker();

        /**
         * Renders sneaker in the ncurses window.
         * @param window - ncurses WINDOW * window where sneaker will be rendered
        */
        void render(WINDOW * window) const final;

        /**
         * Generates path which will enemy follow.
         * Sneaker uses Jarnik's pathfinding.
         * @param gameField instance of GameField, it is used to get tower layout.
        */
        void generatePath(GameField * gameField) final;

    private:
};
