#include "Tank.hpp"
#include "../GameField.hpp"

using namespace std;


Tank::Tank (Coordinate pos, double speed, double maxHealth, double reward) : \
    Enemy(pos, PUNCHER_ID, speed, maxHealth, reward) {

}

Tank::~Tank(){

}

void Tank::render(WINDOW * window) const{
    int tmp = ((int) currentMoveCooldown ) % 4;
    switch (tmp)
    {
    case 0:
        mvwprintw(window, pos.getY() * 3 + 1, pos.getX() * 3, "\\+/");
        mvwprintw(window, pos.getY() * 3 + 2, pos.getX() * 3 + 1, "V");
        break;
    case 2:
        mvwprintw(window, pos.getY() * 3 + 1, pos.getX() * 3, "+/\\");
        mvwprintw(window, pos.getY() * 3 + 2, pos.getX() * 3 + 1, "V");
        break;
    case 1:
    case 3:
        mvwprintw(window, pos.getY() * 3 + 1, pos.getX() * 3, "-+-");
        mvwprintw(window, pos.getY() * 3 + 2, pos.getX() * 3 + 1, "V");
        break;

    default:
        break;
    }
    renderHealthbar(window);
}

void Tank::generatePath(GameField * gameField){

    // Delete previously generated path
    path = deque<Coordinate>();
    bfs(gameField, pos);
}

void Tank::dealDemage(double dmg, GameField * gameField){

    double percentageBefore = health / maxHealth;
    health = health - dmg;
    double percentageAfter = health / maxHealth;

    // Check if tank has crossed a health border, to spawn an enemy
    for(int i = 0; i < TANK_BORDERS_SIZE; i++){
        if(TANK_SPLIT_BORDER[i] <= percentageBefore &&
                TANK_SPLIT_BORDER[i] > percentageAfter){

            // Spawn random enemy at tank location
            gameField->spawnRandomEnemyAtPos(pos);
        }
    }
}
