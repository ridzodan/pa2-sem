#include "Player.hpp"

using namespace std;

Player::Player(){
}

Player::~Player(){

}

double Player::getMoney() const {
    return money;
}

void Player::setMoney(double money){
    this->money = money;
}

void Player::addMoney(double amount){
    money += amount;
}

int Player::getHealth() const{
    return health;
}

void Player::substractHealth(int val){
    this->health = this->health - val;
}

void Player::setHealth(int health){
    this->health = health;
}

void Player::setEnemiesSpawned(int enemiesSpawned){
    this->enemiesSpawned = enemiesSpawned;
}

void Player::addEnemySpawned(){
    enemiesSpawned ++;
}

int Player::getEnemiesSpawned() const{
    return enemiesSpawned;
}

void Player::addToScore(int amount){
    score += amount;
}

int Player::getScore() const {
    return score;
}

bool Player::isDead() const {
    return (health <= 0);
}
