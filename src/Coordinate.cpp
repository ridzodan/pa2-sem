#include "Coordinate.hpp"

using namespace std;

Coordinate::Coordinate(){

}

Coordinate::Coordinate(int x, int y): x(x), y(y){

}

void Coordinate::setX(int x){
    this->x = x;
}

void Coordinate::setY(int y){
    this->y = y;
}

int Coordinate::getX() const {
    return x;
}

int Coordinate::getY() const {
    return y;
}

bool Coordinate::operator == (const Coordinate & c2) const {
    return (x == c2.x && y == c2.y);
}

bool Coordinate::operator != (const Coordinate & c2) const {
    return !(*this == c2);
}

bool Coordinate::operator < (const Coordinate & c2) const {
    return ((x + 1000 * y) < (c2.x + 1000 * c2.y));
}

Coordinate Coordinate::operator + (const Coordinate & c2) const {
    return Coordinate(x + c2.x, y + c2.y);
}

double Coordinate::getDistance(const Coordinate & c1, const Coordinate & c2){
    return sqrt(pow(c1.x - c2.x, 2) + pow(c1.y - c2.y, 2));
}
