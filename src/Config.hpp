#pragma once

// GameField constants
const int LEFT_PANEL_WIDTH = 20;
const int RIGHT_PANEL_WIDTH = 30;
const int TOP_OFFSET = 1;
const int DEFAULT_TIMEOUT = 50;

// Easy difficulty
const int EASY_INITIAL_MONEY = 150;
const int EASY_INITIAL_HEALTH = 30;
const int EASY_SPAWN_SPEED = 55;
const double EASY_HEALTH_COEFICIENT = 1.02;
const int EASY_FIRST_TANK = 30;
const int EASY_TANK_CHANCE = 30;
const double EASY_SCORE_COEFICIENT = 0.5;

// Normal difficulty
const int NORMAL_INITIAL_MONEY = 100;
const int NORMAL_INITIAL_HEALTH = 20;
const int NORMAL_SPAWN_SPEED = 50;
const double NORMAL_HEALTH_COEFICIENT = 1.05;
const int NORMAL_FIRST_TANK = 20;
const int NORMAL_TANK_CHANCE = 25;
const double NORMAL_SCORE_COEFICIENT = 1;

// Hard difficulty
const int HARD_INITIAL_MONEY = 100;
const int HARD_INITIAL_HEALTH = 10;
const int HARD_SPAWN_SPEED = 45;
const double HARD_HEALTH_COEFICIENT = 1.06;
const int HARD_FIRST_TANK = 10;
const int HARD_TANK_CHANCE = 15;
const double HARD_SCORE_COEFICIENT = 1.5;

const int MIN_HEIGHT = 5;
const int MIN_WIDTH = 5;

const char * const MAPS_DIR = "maps";
const char * const EXAMPLE_MAPS_DIR = "examples";
const char * const SAVES_DIR = "saves";

// Towers constants
const int SNIPER_PRICE = 50;
const int SNIPER_DMG = 15;
const int SNIPER_RANGE = 9;
const int SNIPER_RELOAD = 12;

const int DESTROYER_PRICE = 35;
const int DESTROYER_DMG = 20;
const int DESTROYER_RANGE = 5;
const int DESTROYER_RELOAD = 6;

// Enemies constants
const int PUNCHER_SPEED = 2;
const int PUNCHER_HEALTH = 100;
const int PUNCHER_REWARD = 15;

const int SNEAKER_SPEED = 2;
const int SNEAKER_HEALTH = 60;
const int SNEKAER_REWARD = 20;

const int TANK_SPEED = 12;
const int TANK_HEALTH = 1200;
const int TANK_REWARD = 90;
const double TANK_SPLIT_BORDER[3] = {0.8, 0.6, 0.4};
const int TANK_BORDERS_SIZE = 3;

// Object ids (When game is saved, objects are saved under this ids)
const unsigned WALL_ID = 1;
const unsigned TOWER_BASE_ID = 2;
const unsigned SPAWN_ID = 3;
const unsigned BASE_ID = 4;

const unsigned DESTROYER_ID = 10;
const unsigned SNIPER_ID = 11;

const unsigned PUNCHER_ID = 20;
const unsigned SNEAKER_ID = 21;
