#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <ncurses.h>
#include <ctype.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <math.h>

#include "towers/Tower.hpp"
#include "towers/TowerPlace.hpp"
#include "towers/Destroyer.hpp"
#include "towers/Sniper.hpp"
#include "enemies/Puncher.hpp"
#include "enemies/Sneaker.hpp"
#include "enemies/Tank.hpp"
#include "Player.hpp"
#include "Coordinate.hpp"
#include "LevelEditor.hpp"


using namespace std;

/**
 * Class saves all objects which are on the playing field (enemies and towers)
 * + player data and ncurses windows which render game data
 * */
class GameField{

    public:

        GameField();

        GameField(string fileName, int difficulty);

        /**
         * Loads all objects (tower places and walls) from given map file.
         * @param filename path to the map file
         * @returns bool whether the map was succesfully loaded and is valid.
        */
        bool loadBoard(const string & filename);

        /**
         * Loads all objects (towers, enemies and player data) from given save file.
         * @param filename path to game savefile
         * @returns bool whether save was succefully loaded and have valid checksum.
        */
        bool loadSave(const string & filename);

        /** @returns int width of game field (how many towers can be built horizontally) */
        int getWidth() const;

        /** @returns int height of game field (how many towers can be built vertically) */
        int getHeight() const;

        /** @returns ref to Player object */
        Player & getPlayer();

        /** @returns pointer to ncurses WINDOW in which game is rendered */
        WINDOW * getWindow() const;

        /** @returns pointer to ncurses WINDOW in which game stats are rendered */
        WINDOW * getLeftPanel() const;

        /** @returns pointer to ncurses WINDOW in which help text is rendered */
        WINDOW * getRightPanel() const;

        /**
         * Method renders all objects on the screen
         * @param c last clicked button - just for development
        */
        void render(char c);

        /**
         * Method builds new Tower on given coords if all checks are valid (money and coords).
         * @param x - x coordinate
         * @param y - y coordinate
         * @param type - which tower is beeing bought ('D' - Destroyer, 'S' - Sniper)
        */
        void buyTower(Coordinate pos, char type);

        /**
         * Increases all cooldowns in the game.
        */
        void increaseReloads();

        /**
         * If spawn cooldown is ready spawns enemy at random spawn.
        */
        void spawnEnemy();

        /**
         * Move enemies which have ready move cooldown.
        */
        void moveEnemies();

        /** @returns tower which is player's base */
        TowerPlace * getBase() const;

        /**
         * Gets tower on given coordinate
         * @param pos - coordinate of the tower
         * @returns pointer to tower that is on the coords, if none returns NULL
        */
        Tower * getTowerAt(Coordinate pos) const;

        /**
         * Loops through enemies and check if they reached destination (base) / the are dead.
        */
        void checkEnemies();

        /**
         * Make towers which have ready shoot cooldown to shoot at enemy.
        */
        void towersShoot();

        /** @returns vector of enemies */
        const vector<Enemy *> & getEnemies() const;

        /**
         * Finds first available save name and saves game progress to it.
        */
        void saveGame();

        /**
         * Gets field rating on given coordinate
         * (How dangerous place is for enemy)
         * @param pos - coordinate of the searched point
         * @returns place rating, 0 if coord doesnt exist.
         */
        double getRatingAt(Coordinate pos) const;

        /**
         * Sets game field to build mode.
        */
        void setBuildMode();

        /**
         * @returns true if game field is in build mode.
        */
        bool isBuildMode() const;

        /**
         * Sets game field to play mode.
        */
        void setPlayMode();

        /**
         * @returns true if game field is in play mode.
        */
        bool isPlayMode() const;

        /**
         * Sets game field to tower upgrade mode.
        */
        void setUpgradeMode();

        /**
         * @returns true if game field is in tower upgrade mode.
        */
        bool isUpgradeMode() const;

        /**
         * Deletes all allocated memory
        */
        void destruct();

        string getMapName() const;

        string getSaveName() const;

        /**
         * When game field is in upgrade mode, sets tower which is under cursor now.
         * @param tower - tower which is currently under cursor
        */
        void setCursorOn(Tower * tower);

        /**
         * If tower is under cursor and player has enough money, selected tower is upgraded.
        */
        void upgradeSelectedTower();

        int getDifficulty() const;

        /**
         * Spawns random enemy (except tank), at given coords.
         * @param pos - where enemy will be spawned.
        */
        void spawnRandomEnemyAtPos(Coordinate pos);

    protected:

        int width = 0; /**< width of game field */
        int height = 0; /**< height of game field */

        /**
         * Contains all towers saved in a map by their coordinates,
         * */
        map<Coordinate, Tower *> towersMap;

        Player player; /**< object contains game info about player (health, money, ...) */
        WINDOW * window = NULL; /**< ncurses WINDOW in which game is rendered */
        WINDOW * leftPanel = NULL; /**< ncurses WINDOW in which game stats are rendered */
        WINDOW * rightPanel = NULL; /**< ncurses WINDOW in which help text is rendered */
        TowerPlace * base = NULL; /**< tower which is player's base */
        vector<Tower *> spawns; /**< vector of towers which are enemies spawns */
        vector<Enemy *> enemies; /**< vector of enemies */

        /** Each field on the map has rating which is used for Jarnik's pathfinding (More towers in range -> worse) */
        map<Coordinate, double> mapRating;

        char gameState = 'G'; /**< Current state of the game ('G' - game, 'B' - build mode) */

        int currentSpawnReload = 0; /**< current reload of enemies spawn */

        // coeficients which are based on difficulty
        int spawnReload; /**< how fast enemies will spawn */
        double healthCoeficient; /**< how much are enemies getting tougher */
        int firstTank; /**< when is the first time when tank can spawn (after how much enemies) */
        int tankChance; /**< when tank can already spawn, big is change, that he can spawn (1/n chance) */
        double scoreCoeficient;


        Tower * cursorOn = NULL; /**< It is used when upgrading a tower */

        string mapName = "";
        string saveName = "";

        int difficulty = 1; /**< difficulty of the game */

        /**
         * Loads wall from mapfile
         * @param inStream input stream from which wall attributes are loaded
        */
        void loadWall(ifstream & inStream);

        /**
         * Loads platform where towers can be later built from mapfile
         * @param inStream input stream from which tower place attributes are loaded
        */
        void loadPlaceForTower(ifstream & inStream);

        /**
         * Loads spawn of enemies from mapfile
         * @param inStream input stream from which spawn attributes are loaded
        */
        void loadSpawn(ifstream & inStream);

        /**
         * Loads player base from mapfile
         * @param inStream input stream from which base attributes are loaded
        */
        void loadBase(ifstream & inStream);

        /**
         * Checks if coordinate is inside playing field
         * @returns bool whether playing field contains given coordinate
        */
        bool checkCoordinate(Coordinate pos) const;

        /**
         * Renders game info on left side panel
         * @param c - last clicked key (just for development)
        */
        void renderLeftPanel(char c);

        /**
         * Renders help info on right side panel
        */
        void renderRightPanel();

        /**
         * Renders game objects (towers & enemies) on main window
        */
        void renderWindow();

        /**
         * Wipes all chars on left panel
        */
        void clearLeftPanel();

        /**
         * Wipes all chars on right panel
        */
        void clearRightPanel();

        /**
         * Wipes all chars on main window
        */
        void clearWindow();

        /**
         * Adds new tower to both stl containers (vector + map)
         * @param tower - tower which will be saved.
        */
        void addTower(Tower * tower);

        /**
         * Adds tower rating to rating map,
         * Rating map is used in pathfinding to avoid towers.
         * (Path of the least resistance.)
        */
        void addTowerToRating(Tower * tower);

        /**
         * Saves game progress to file
         * @param filename - to which file game will be saved
        */
        void saveGame(const string & filename);

        /**
         * Loads enemy from save file.
         * @param inStream - filestream from which enemy will be loaded
         * @param id - id of enemy (from it will be determined what enemy should be rendered)
         * @returns created enemy.
        */
        Enemy * loadEnemy(ifstream & inStream, unsigned id) const;

        /**
         * Loads tower from save file.
         * @param inStream - filestream from which tower will be loaded
         * @param id - id of tower (from it will be determined what tower should be rendered)
         * @returns created tower.
        */
        Tower * loadTower(ifstream & inStream, unsigned id);

        /**
         * Based on difficulty loads multipliers and sets initial values.
        */
        void processDifficulty();
};
