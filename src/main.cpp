/** @file */
#include <ncurses.h>
#include <ctype.h>
#include <stdio.h>
#include <vector>
#include <sstream>
#include <locale.h>
#include <wchar.h>

#include "Game.hpp"

using namespace std;

int main () {

    Game g;
    g.run();

    return 0;
}

/*!
 * \mainpage
 * Author: Daniel Ridzoň \n
 * I already hear you asking, yes, it's legit name of the game.
 *
 * \section Progtest_theme
 *
 * \subsection Tower_defense
 *
 * Naprogramujte jendoduchou grafickou tower defense hru.
 *
 * Váš engine:
 * \li ze souboru nahraje definici věží (cena, velikost, útok, dosah, ...)
 * \li ze souboru nahraje možné mapy a typy nepřátel (životy, rychlost, odolnost na určitý typ věže ,...)
 * \li implementuje jednoduchou interakci věž vs. útočníci (útok, proběhnutí, ...), počitadlo skóre, detekci vítězství (po nezabití x útočníků)
 * \li implementuje alespoň 2 strategie průchodu hracím polem
 * \li umožňuje ukládat a načítat rozehrané hry
 *
 * Engine může fungovat jako real-time hra, či tahová hra. \n
 * Cesta kudy se budu útočníci ubírat bude vždy nejkratší možná vzhledem ke zdím a věžím
 *
 * Kde lze využít polymorfismus? (doporučené):
 * \li Parametry věží: znak, barva, dosah, zranění, ...
 * \li Efekty útoku věží: zranění, zpomalení, ...
 * \li Políčka mapy: volno, věž, útočník ...
 * \li Strategie hledání cesty: předdefinovaná cesta, nejkratší (BFS, případně náhodná odchylka), A* prohledávání, vyhýbání se věžím
 * \li Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), ...
 *
 * \section My_implementation
 *
 * \subsection intro
 *
 * I have decided, that I will use ncurses library for rendering the game. \n
 * But because I didn't want it to be low res game, where each object is just one pixel (char),
 * every object in the game is rendered on 3x3 matrix.
 * Its adventages are, that I can show where tower canons aim and show healthbar above enemies.
 * The disadventage of this approach is that the game map can't be be vertically
 * very high. Fortunately the game is still playable and enjoyable.
 * For the best playing experience, I advice to play the game in the fullscreen terminal.
 *
 * \subsection Enemy_pathfinding
 *
 * Game supports multiple enemy spawns.
 * When is the time to spawn an enemy, game chooses random spawn, pseudorandom enemy
 * and on position of the spawn creates instance of the enemy.
 * With every spawned enemy, the game difficulty increases and enemies are tougher. \n
 * Game implements these two pathfinding alghoritms:
 * \li BFS - Enemy goes always the shortest possible route to the players base.
 * (In the implementation I use an element of randomness, when BFS alghoritm looks for
 * unvisited node neighbours, it checks them in the random order.
 * Although there is element of randomness, it still finds the shortest route possible).
 * \li Modified Jarniks alghoritm - Enemies search for optimal route, on which they
 * can be hitted the least amount of demage.
 *
 * The pathfinding alghoritm is called just twice, when enemy is spawned
 * and once again if a new tower is built.
 *
 * \subsection Polymorfism_design
 *
 * I use polymorfism on all objects which are beeing rendered on the game field
 * (Enemies and towers). \n
 *
 * Base virtual class GameObject has two pure virtual methdos:
 * \li render - renders child class instance on the screen
 * \li toString - converts child class instance to string (It is used to save game progress
 * to a savefile)
 *
 * GameObject class is extended by two more virtual classes - Enemy and Tower,
 * which extend GameObject virtual methods interface by methods, which are specific just
 * for towers or enemies. \n
 *
 * Virtual methods of Enemy :
 * \li generatePath - uses pathfinding alghoritms to generate path which enemy instance follows
 * \li dealDemage - on basic enemies just substracts their health, Tank overrides this method,
 * when his health crosses specified borders, he spawns another enemies.
 *
 * Virtual methods of Tower :
 * \li shoot - tower chooses the nearest enemy and shoots at him. If tower is
 * pasive (TowerPlace - wall, spawn, base, base of tower) nothing happens to
 * save computation time.
 * \li getDemageUpgradeBenefits
 * \li getRangeUpgradeBenefits
 * \li getReloadUpgradeBenefits
 * \li getUpgradeCost               - Each type of a tower has its own upgrade pattern.
 *
 * \subsection Game_saving
 *
 * Every source file, which is loaded by the game (maps, save files, high score file)
 * has at the end checksum. Before program extracts data from these files,
 * checksum is always checked.
 *
 * Save files and highscore files therefore cannot be edited. Otherwise they wont be loaded.
 *
 * Because player cannot create or edit map files manualy,
 * game contains LevelEditor, which can create a new map or modify existing one.
 */
