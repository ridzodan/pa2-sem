#include "GameObject.hpp"

using namespace std;


GameObject::GameObject (Coordinate pos, unsigned objectId) : pos(pos), objectId(objectId) {

    init_pair (GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair (RED, COLOR_RED, COLOR_BLACK);
    init_pair (BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair (MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
    init_pair (CYAN, COLOR_CYAN, COLOR_BLACK);
    init_pair (YELLOW, COLOR_YELLOW, COLOR_BLACK);

}

Coordinate GameObject::getPos() const{
    return pos;
}

void GameObject::setPos(Coordinate pos){
    this->pos = pos;
}
