#include "Sniper.hpp"

using namespace std;


Sniper::Sniper (Coordinate pos, double range, double demage, double reloadTime)
    : Tower(pos, SNIPER_ID, range, demage, reloadTime) {

}

Sniper::~Sniper(){

}

void Sniper::render(WINDOW * window) const{

    int x = pos.getX();
    int y = pos.getY();

    mvwprintw(window, y * 3, x * 3,     "┌─┐");
    mvwprintw(window, y * 3 + 1, x * 3, "│S│");
    mvwprintw(window, y * 3 + 2, x * 3, "└─┘");

    renderCanon(window);
}

double Sniper::getDemageUpgradeBenefits() const {
    if(upgradeLevel % 2 == 1){
        return demage * 0.2;
    }
    return 0;
}

double Sniper::getRangeUpgradeBenefits() const {
    if(upgradeLevel % 2 == 0){
        return range * 0.3;
    }
    return 0;
}

double Sniper::getReloadUpgradeBenefits() const {
    return 0;
}

double Sniper::getUpgradeCost() const {
    return (SNIPER_PRICE / 2.0 ) + upgradeLevel * 16;
}
