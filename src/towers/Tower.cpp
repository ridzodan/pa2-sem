#include "Tower.hpp"
#include "../GameField.hpp"

#include <math.h>

using namespace std;


Tower::Tower (Coordinate pos, unsigned objectId, double range, double demage, double reloadTime)
    : GameObject(pos, objectId), range(range), demage(demage), reloadTime(reloadTime) {

}

Tower::Tower (Coordinate pos, unsigned objectId)
    : GameObject(pos, objectId) {

}

Tower::~Tower(){

}

bool Tower::IsBuildable() const{
    return (objectId == TOWER_BASE_ID);
}

void Tower::increaseReload(){
    currentReload ++;
}

void Tower::shoot(GameField * gameField){ // TODO later can be implemeted aiming not only at the nearest tower

    // Check if tower can shoot
    if(currentReload < reloadTime){
        return;
    }

    // Check if target is in range
    if(target != NULL && getDistance(target) > range){
        target = NULL;
    }

    // If target == NULL, find new target
    if(target == NULL){
        target = findTarget(gameField);

        // Check if taget has been found
        if(target == NULL){
            return;
        }
    }
    currentReload = 0;

    target->dealDemage(demage, gameField);
}

Enemy * Tower::getTarget() const{
    return target;
}

void Tower::setTarget(Enemy * enemy){
    this->target = enemy;
}

double Tower::getDistance(Enemy * enemy){

    return Coordinate::getDistance(pos, enemy->getPos());
}

Enemy * Tower::findTarget(GameField * gameField){

    double minDist = range + 0.1;
    Enemy * best = NULL;

    for(Enemy * enemy: gameField->getEnemies()){
        double distance = getDistance(enemy);
        if(distance < minDist){
            best = enemy;
            minDist = distance;
        }
    }

    return best;
}

void Tower::renderCanon(WINDOW * window) const{

    int x = pos.getX();
    int y = pos.getY();

    double canonRotation; // Angle to enemy in radians

    // Get canon rotation
    if(target == NULL){
        canonRotation = - (PI / 2.0);
    }
    else{
        canonRotation = atan2(y - target->getPos().getY(), x - target->getPos().getX());
    }
    if(canonRotation < 0){
        canonRotation = canonRotation + (2.0 * PI);
    }
    canonRotation = (2.0 * PI) - canonRotation;

    // Print coresponding canon
    if(canonRotation <= PI / 8.0 || canonRotation > 15.0 * PI / 8.0 ){
        mvwprintw(window, y * 3 + 1, x * 3, "─");
    }
    else if(canonRotation > PI / 8.0 && canonRotation <= (3.0 * PI) / 8.0){
        mvwprintw(window, y * 3 + 2, x * 3, "╱");
    }
    else if(canonRotation > (3.0 * PI) / 8.0 && canonRotation <= (5.0 * PI) / 8.0){
        mvwprintw(window, y * 3 + 2, x * 3 + 1, "│");
    }
    else if(canonRotation > (5.0 * PI) / 8.0 && canonRotation <= (7.0 * PI) / 8.0){
        mvwprintw(window, y * 3 + 2, x * 3 + 2, "╲");
    }
    else if(canonRotation > (7.0 * PI) / 8.0 && canonRotation <= (9.0 * PI) / 8.0){
        mvwprintw(window, y * 3 + 1, x * 3 + 2, "─");
    }
    else if(canonRotation > (9.0 * PI) / 8.0 && canonRotation <= (11.0 * PI) / 8.0){
        mvwprintw(window, y * 3, x * 3 + 2, "╱");
    }
    else if(canonRotation > (11.0 * PI) / 8.0 && canonRotation <= (13.0 * PI) / 8.0){
        mvwprintw(window, y * 3, x * 3 + 1, "│");
    }
    else if(canonRotation > (13.0 * PI) / 8.0 && canonRotation <= (15.0 * PI) / 8.0){
        mvwprintw(window, y * 3, x * 3, "╲");
    }

}

string Tower::toString(){

    stringstream res;

    res << " " << objectId << " ";

    res << pos.getX() << " " << pos.getY() << " ";
    res << range << " " << demage << " " << reloadTime << " " << currentReload;
    res << " " << upgradeLevel;

    return res.str();
}

void Tower::setCurrentReload(double reload){
    currentReload = reload;
}

double Tower::getRange() const{
    return range;
}

double Tower::getDemage() const{
    return demage;
}

double Tower::getReload() const{
    return reloadTime;
}

double Tower::getDemageUpgradeBenefits() const {
    return 0;
}

double Tower::getRangeUpgradeBenefits() const {
    return 0;
}

double Tower::getReloadUpgradeBenefits() const {
    return 0;
}

double Tower::getUpgradeCost() const {
    return 0;
}

void Tower::upgrade(){
    demage = demage + getDemageUpgradeBenefits();
    range = range + getRangeUpgradeBenefits();
    reloadTime = reloadTime - getReloadUpgradeBenefits();

    upgradeLevel ++;
}

void Tower::setUpgradeLevel(int upgradeLevel){
    this->upgradeLevel = upgradeLevel;
}

void Tower::renderRangeIndicator(int width, int height, WINDOW * window) const{
    for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
            if(Coordinate::getDistance(Coordinate(x, y), pos) <= range){
                mvwprintw(window, y * 3 + 1, x * 3 + 1, ".");
            }
        }
    }
}
