#pragma once

#include "Tower.hpp"

using namespace std;

/**
 * Class represents 4 passive towers:
 * Wall
 * Tower base - player can build new tower on this place
 * Spawn - Enemies will spawn on this place
 * Base - Target destination of the enemies
*/
class TowerPlace : public Tower{

    public:

        TowerPlace(Coordinate pos, unsigned objectId);

        ~TowerPlace();

        void render(WINDOW * window) const final;

        void shoot(GameField * GameField) final;

    private:
};
