#pragma once

#include "Tower.hpp"

using namespace std;

/** More advanced tower, has bigger range and demage but long reload time. */
class Sniper : public Tower{

    public:

        /**
         * Constructor
         * @param pos - coordinate of the tower
         * @param range - range of the tower
         * @param demage - demage of the tower
         * @param reloadTime - how fast can tower shoot (lower ~ faster)
         */
        Sniper(Coordinate pos, double range, double demage, double reloadTime);

        ~Sniper();

        /**
         * Renders object in the ncurses window.
         * @param window - ncurses WINDOW * window where object will be rendered
        */
        void render(WINDOW * window) const final;

        virtual double getDemageUpgradeBenefits() const final;

        virtual double getRangeUpgradeBenefits() const final;

        virtual double getReloadUpgradeBenefits() const final;

        virtual double getUpgradeCost() const final;

    private:

};
