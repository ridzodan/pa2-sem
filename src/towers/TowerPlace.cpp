#include <iostream>

#include "TowerPlace.hpp"

using namespace std;

TowerPlace::TowerPlace(Coordinate pos, unsigned objectId)
    : Tower(pos, objectId) {

}

TowerPlace::~TowerPlace(){

}

void TowerPlace::render(WINDOW * window) const{

    int x = pos.getX();
    int y = pos.getY();

    switch(objectId){
        case SPAWN_ID:
            wattron (window, COLOR_PAIR( RED ));
            mvwprintw(window, y * 3, x * 3,     "╝ ╚");
            mvwprintw(window, y * 3 + 1, x * 3, " + ");
            mvwprintw(window, y * 3 + 2, x * 3, "╗ ╔");
            wattroff (window, COLOR_PAIR( RED ));
            break;
        case BASE_ID:
            wattron (window, COLOR_PAIR( GREEN ));
            mvwprintw(window, y * 3, x * 3,     "╔╦╗");
            mvwprintw(window, y * 3 + 1, x * 3, "╠╬╣");
            mvwprintw(window, y * 3 + 2, x * 3, "╚╩╝");
            wattroff (window, COLOR_PAIR( GREEN ));
            break;
        case TOWER_BASE_ID:
            mvwprintw(window, y * 3, x * 3,     "┌ ┐");

            mvwprintw(window, y * 3 + 2, x * 3, "└ ┘");
            break;
        case WALL_ID:
        default:
            mvwprintw(window, y * 3, x * 3,     "┌─┐");
            mvwprintw(window, y * 3 + 1, x * 3, "│█│");
            mvwprintw(window, y * 3 + 2, x * 3, "└─┘");
            break;
    }
}

void TowerPlace::shoot(GameField *) {
}
