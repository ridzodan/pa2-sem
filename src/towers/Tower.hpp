#pragma once

#include "../GameObject.hpp"
#include "../enemies/Enemy.hpp"

using namespace std;

#define PI 3.1415926535

// Forward declaration to prevent circular dependency
class GameField;

/**
 * Virtual class, that is inherited by all turrets and passive towers(wall).
*/
class Tower : public GameObject{

    public:

        /**
         * Constructor
         * @param pos - coordinate of the tower
         * @param objectId - id under which object will be saved to save file
         * @param range - range of the tower
         * @param demage - demage of the tower
         * @param reloadTime - how fast can tower shoot (lower ~ faster)
         */
        Tower(Coordinate pos, unsigned objectId, double range, double demage, double reloadTime);

        /**
         * Constructor
         * @param pos - coordinate of the tower
         * @param objectId - id under which object will be saved to save file
         */
        Tower(Coordinate pos, unsigned objectId);

        ~Tower();

        /**
         * Renders object in the ncurses window.
         * @param window - ncurses WINDOW * window where object will be rendered
        */
        virtual void render(WINDOW * window) const = 0;

        /**
         * @returns true if tower can be built on this coordinate.
        */
        bool IsBuildable() const;

        /**
         * Adds one point to tower shooting cooldown.
         * (When currentReload reaches reloadTime,
         * tower can shoot and currentColdown is reset to 0)
        */
        void increaseReload();

        /**
         * If cooldown is ready and enemy in range, deals demage to enemy.
         * When last enemy target is not in range, also looks for a new target.
         * @param gameField instance of gameField, which is needed when finding a target.
        */
        virtual void shoot(GameField * gameField);

        /**
         * @returns pointer to enemy which is current target of the tower.
        */
        Enemy * getTarget() const;

        /**
         * @param enemy new target of the tower, can be set to NULL.
        */
        void setTarget(Enemy * enemy);

        /**
         * Returns tower representation in a string format.
         * It is used when saving object to save file.
        */
        virtual string toString() final;

        void setCurrentReload(double reload);

        double getRange() const;

        double getDemage() const;

        double getReload() const;

        virtual double getDemageUpgradeBenefits() const;

        virtual double getRangeUpgradeBenefits() const;

        virtual double getReloadUpgradeBenefits() const;

        virtual double getUpgradeCost() const;

        /**
         * Upgrades tower to the next level
         * (Enhances tower stats based on tower type)
        */
        void upgrade();

        void setUpgradeLevel(int upgradeLevel);

        /**
         * Renders dots on the map, where tower can shoot.
        */
        void renderRangeIndicator(int width, int height, WINDOW * window) const;

    protected:

        double range = 0; /**< Range of the tower */
        double demage = 0; /**< How much demage tower deals to enemies */

        double reloadTime = 0; /**< How often can tower shoot (lower ~ faster) */
        double currentReload = 0;
        /**< Current state of shooting reload (when reaches reloadTime, tower can shoot) */

         /**< Angle to target in radians */
        Enemy * target = NULL; /**< Enemy target, which tower aims at. */

        int upgradeLevel = 1; /**< Level of tower, it is used to compute upgrade benefits */

        /**
         * Counts the distance between the tower and enemy
         * @param enemy enemy which is beeing measured.
         * @returns how far enemy is from the tower.
        */
        double getDistance(Enemy * enemy);

        /**
         * Finds nearest enemy from the tower.
         * @param gameField instance of GameField, from which enemies are getted.
         * @returns nearest enemy, NULL if none in range.
        */
        Enemy * findTarget(GameField * gameField);

        /**
         * Renders tower canon in the ncurses window.
        */
        void renderCanon(WINDOW * window) const;

};
