#pragma once

#include "Tower.hpp"

using namespace std;

/** Basic tower which has mid range and low demage */
class Destroyer : public Tower{

    public:

        /**
         * Constructor
         * @param pos - coordinate of the tower
         * @param range - range of the tower
         * @param demage - demage of the tower
         * @param reloadTime - how fast can tower shoot (lower ~ faster)
         */
        Destroyer(Coordinate pos, double range, double demage, double reloadTime);

        ~Destroyer();

        /**
         * Renders object in the ncurses window.
         * @param window - ncurses WINDOW * window where object will be rendered
        */
        void render(WINDOW * window) const final;

        virtual double getDemageUpgradeBenefits() const final;

        virtual double getRangeUpgradeBenefits() const final;

        virtual double getReloadUpgradeBenefits() const final;

        virtual double getUpgradeCost() const final;

    private:

};
