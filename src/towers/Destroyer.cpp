#include "Destroyer.hpp"

using namespace std;


Destroyer::Destroyer (Coordinate pos, double range, double demage, double reloadTime)
    : Tower(pos, DESTROYER_ID, range, demage, reloadTime) {

}

Destroyer::~Destroyer(){

}

void Destroyer::render(WINDOW * window) const{

    int x = pos.getX();
    int y = pos.getY();

    mvwprintw(window, y * 3, x * 3,     "┌─┐");
    mvwprintw(window, y * 3 + 1, x * 3, "│D│");
    mvwprintw(window, y * 3 + 2, x * 3, "└─┘");

    renderCanon(window);
}

double Destroyer::getDemageUpgradeBenefits() const {
    return demage * 0.1;
}

double Destroyer::getRangeUpgradeBenefits() const {
    return range * 0.1;
}

double Destroyer::getReloadUpgradeBenefits() const {
    return reloadTime * 0.05;
}

double Destroyer::getUpgradeCost() const {
    return (DESTROYER_PRICE / 3.0 ) + upgradeLevel * 20;
}
