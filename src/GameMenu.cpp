#include "GameMenu.hpp"

using namespace std;

GameMenu::GameMenu(vector<string> & menuChoices, string title)
        : menuChoices(menuChoices), title(title){
    init_pair (COLOR_SELECTED, COLOR_BLACK, COLOR_WHITE);
    showMenu();
}

GameMenu::~GameMenu(){
    hideMenu();
}

int GameMenu::getChoice(string & input){

    int c = 0;

    while(true){

        // Wait for input from player
        c=getch();

        // Handle key enter
        if(c == 10){

            // Check if current position is input field
            if(!(menuChoices[currentChoice] == "%input%")){
                break;
            }else{
                continue;
            }
        }

        switch(c){

            case -1: // Nothing pressed
                break;

            case KEY_UP:
                currentChoice = (currentChoice - 1 + menuChoices.size()) % menuChoices.size();
                break;

            case KEY_DOWN:
                currentChoice = (currentChoice + 1) % menuChoices.size();
                break;

            case KEY_BACKSPACE:
                if(menuChoices[currentChoice] == "%input%" && !input.empty()){
                    input.pop_back();
                    inputValue = input;
                }
                break;

            default:
                if(menuChoices[currentChoice] == "%input%" && (isalnum(c) || c == '_')){
                    input.push_back(c);
                    inputValue = input;
                }
                break;
        }
        renderChoices();
    }
    hideMenu();
    return currentChoice;
}

int GameMenu::getChoice(){

    string tmp;
    return getChoice(tmp);
}

void GameMenu::showMenu(){

    if(window == NULL){
        height = COLS;
        width = LINES - 4;

        window = newwin(width, height, 2, 0);
        box(window, 0, 0);
        wrefresh(window);

        renderChoices();
    }
}

void GameMenu::hideMenu(){

    if(window != NULL){
        werase(window);
        wrefresh(window);
        delwin(window);

        window = NULL;
    }
}

void GameMenu::renderChoices(){

    // Erase previous content
    for(int i = 0; i < width; i++){
        for(int k = 0; k < height; k++){
            mvwaddch(window, k, i, ' ');
        }
    }
    box(window, 0, 0);

    // Render title
    mvwaddstr(window, 2, 15, title.c_str());

    int choicesOffset = 4;

    // Render choices
    for(unsigned i = 0; i < menuChoices.size(); i++){

        if(i == (unsigned) currentChoice){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
        }

        // Check if rendering input field
        if(menuChoices[i] == "%input%"){
            if(inputValue == ""){
                mvwaddstr(window, i * 2 + choicesOffset, 2, "[input]");
            }
            else{
                mvwaddstr(window, i * 2 + choicesOffset, 2, inputValue.c_str());
            }
        }
        else{
            mvwaddstr(window, i * 2 + choicesOffset, 2, menuChoices[i].c_str());
        }

        if(i == (unsigned) currentChoice){
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
    }
    wrefresh(window);
}
