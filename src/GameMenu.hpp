#pragma once

#include <ncurses.h>
#include <string>
#include <vector>

#define COLOR_SELECTED 2

using namespace std;

/**
 * Utility to display choice lists
*/
class GameMenu{
    public:

        /**
         * Constructor
         * @param menuChoices - choices which will be displayed
         * @param title - title which will be displayed above the choices
        */
        GameMenu(vector<string> & menuChoices, string title);

        ~GameMenu();

        /**
         * Contains logic which process players selection input
         * @returns index of a choice which player have selected.
        */
        int getChoice();

        /**
         * Contains logic which process players selection input
         * @returns index of a choice which player have selected.
         * @param[out] input - if choices contains option %input% returns value which player has inputed
        */
        int getChoice(string & input);

        /**
         * Hides ncurses windows which have been created to display choices
        */
        void hideMenu();

        /**
         * Shows ncurses windows which contains choices list.
        */
        void showMenu();

    private:

        WINDOW * window = NULL;
        vector<string> & menuChoices;
        int currentChoice = 0;
        int width = 0;
        int height = 0;

        string inputValue = "";
        string title = "";

        /**
         * Renders choices list on the screen
         * Marks selected choice by inverted colors.
        */
        void renderChoices();

};
