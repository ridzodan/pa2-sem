# VARIABLES
CXX = g++
CXX_FLAGS = -Wall -pedantic -Wextra -g -std=c++11 -lncursesw
BUILD_DIR = build
MKDIR = mkdir -p

# COMMANDS

## compile code
.PHONY: all
all: $(BUILD_DIR)/ridzodan

## compile code #2 warum?
.PHONY: compile
compile: $(BUILD_DIR)/ridzodan
	@cp $(BUILD_DIR)/ridzodan ridzodan

## turn on program
.PHONY: run
run: $(BUILD_DIR)/ridzodan
	./$(BUILD_DIR)/ridzodan

## Generate documentation
.PHONY: doc
doc:
	@doxygen Doxyfile

## Delete genereated files
.PHONY: clean
clean:
	if [ -d "./$(BUILD_DIR)" ]; then rm -r ./$(BUILD_DIR); fi
	if [ -d "./doc" ]; then rm -r ./doc; fi
	if [ -d "./saves" ]; then rm -r ./saves; fi
	if [ -d "./maps" ]; then rm -r ./maps; fi
	if [ -f "./ridzodan" ]; then rm ./ridzodan; fi

## Program 'ridzodan'
$(BUILD_DIR)/ridzodan: $(BUILD_DIR)/main.o $(BUILD_DIR)/towers/Destroyer.o $(BUILD_DIR)/towers/Tower.o \
		$(BUILD_DIR)/Game.o $(BUILD_DIR)/GameField.o $(BUILD_DIR)/GameObject.o \
		$(BUILD_DIR)/towers/TowerPlace.o $(BUILD_DIR)/Player.o $(BUILD_DIR)/enemies/Enemy.o \
		$(BUILD_DIR)/towers/Sniper.o $(BUILD_DIR)/enemies/Puncher.o \
		$(BUILD_DIR)/enemies/Sneaker.o $(BUILD_DIR)/GameMenu.o $(BUILD_DIR)/Coordinate.o \
		$(BUILD_DIR)/LevelEditor.o $(BUILD_DIR)/enemies/Tank.o
	$(MKDIR) $(BUILD_DIR)
	$(MKDIR) $(BUILD_DIR)/enemies
	$(MKDIR) $(BUILD_DIR)/towers
	$(MKDIR) saves
	$(MKDIR) maps
	$(CXX) $(CXX_FLAGS) $^ -o $@ -fsanitize=address


# COMPILE FILES
$(BUILD_DIR)/towers/%.o: src/towers/%.cpp
	$(MKDIR) $(BUILD_DIR)/towers
	$(CXX) $(CXX_FLAGS) $< -c -o $@

$(BUILD_DIR)/enemies/%.o: src/enemies/%.cpp
	$(MKDIR) $(BUILD_DIR)/enemies
	$(CXX) $(CXX_FLAGS) $< -c -o $@

$(BUILD_DIR)/%.o: src/%.cpp
	$(MKDIR) $(BUILD_DIR)
	$(CXX) $(CXX_FLAGS) $< -c -o $@

## ($@ target file,  $< first dependency, $^ all dependencies)
# DEPENDENCIES
$(BUILD_DIR)/main.o: src/main.cpp src/Game.hpp src/towers/Tower.hpp \
	src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/enemies/Enemy.hpp \
	src/GameField.hpp src/towers/TowerPlace.hpp src/towers/Destroyer.hpp \
	src/towers/Sniper.hpp src/enemies/Puncher.hpp src/enemies/Sneaker.hpp \
	src/Player.hpp src/GameMenu.hpp src/enemies/Tank.hpp
$(BUILD_DIR)/Coordinate.o: src/Coordinate.cpp src/Coordinate.hpp
$(BUILD_DIR)/Game.o: src/Game.cpp src/Game.hpp src/towers/Tower.hpp \
	src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/enemies/Enemy.hpp \
	src/GameField.hpp src/towers/TowerPlace.hpp src/towers/Destroyer.hpp \
	src/towers/Sniper.hpp src/enemies/Puncher.hpp src/enemies/Sneaker.hpp \
	src/Player.hpp src/GameMenu.hpp src/LevelEditor.hpp src/enemies/Tank.hpp
$(BUILD_DIR)/GameField.o: src/GameField.cpp src/GameField.hpp src/towers/Tower.hpp \
	src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/enemies/Enemy.hpp \
	src/towers/TowerPlace.hpp src/towers/Destroyer.hpp src/towers/Sniper.hpp \
	src/enemies/Puncher.hpp src/enemies/Sneaker.hpp src/Player.hpp src/LevelEditor.hpp \
	src/enemies/Tank.hpp
$(BUILD_DIR)/GameMenu.o: src/GameMenu.cpp src/GameMenu.hpp
$(BUILD_DIR)/GameObject.o: src/GameObject.cpp src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp
$(BUILD_DIR)/Player.o: src/Player.cpp src/Player.hpp
$(BUILD_DIR)/LevelEditor.o: src/LevelEditor.cpp src/LevelEditor.hpp src/Config.hpp \
	src/Coordinate.hpp

$(BUILD_DIR)/enemies/Enemy.o: src/enemies/Enemy.cpp src/enemies/Enemy.hpp \
	src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/GameField.hpp \
	src/towers/Tower.hpp src/towers/TowerPlace.hpp \
	src/towers/Destroyer.hpp src/towers/Sniper.hpp \
	src/enemies/Puncher.hpp src/enemies/Sneaker.hpp \
	src/Player.hpp src/LevelEditor.hpp
$(BUILD_DIR)/enemies/Puncher.o: src/enemies/Puncher.cpp src/enemies/Puncher.hpp \
	src/enemies/Enemy.hpp src/GameObject.hpp \
	src/Config.hpp src/Coordinate.hpp
$(BUILD_DIR)/enemies/Sneaker.o: src/enemies/Sneaker.cpp src/enemies/Sneaker.hpp \
	src/enemies/Enemy.hpp src/GameObject.hpp \
	src/Config.hpp src/Coordinate.hpp \
	src/GameField.hpp src/towers/Tower.hpp \
	src/towers/TowerPlace.hpp src/towers/Destroyer.hpp \
	src/towers/Sniper.hpp src/enemies/Puncher.hpp \
	src/Player.hpp src/LevelEditor.hpp src/enemies/Tank.hpp
$(BUILD_DIR)/enemies/Tank.o: src/enemies/Tank.cpp src/enemies/Tank.hpp \
	src/enemies/Enemy.hpp src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/GameField.hpp \
	src/towers/Tower.hpp src/towers/TowerPlace.hpp \
	src/towers/Destroyer.hpp src/towers/Sniper.hpp \
	src/enemies/Puncher.hpp src/enemies/Sneaker.hpp \
	src/Player.hpp src/LevelEditor.hpp


$(BUILD_DIR)/towers/Destroyer.o: src/towers/Destroyer.cpp src/towers/Destroyer.hpp \
	src/towers/Tower.hpp src/GameObject.hpp \
	src/Config.hpp src/Coordinate.hpp \
	src/enemies/Enemy.hpp
$(BUILD_DIR)/towers/Sniper.o: src/towers/Sniper.cpp src/towers/Sniper.hpp \
	src/towers/Tower.hpp src/GameObject.hpp \
	src/Config.hpp src/Coordinate.hpp \
	src/enemies/Enemy.hpp
$(BUILD_DIR)/towers/Tower.o: src/towers/Tower.cpp src/towers/Tower.hpp \
	src/GameObject.hpp src/Config.hpp \
	src/Coordinate.hpp src/enemies/Enemy.hpp \
	src/GameField.hpp src/towers/TowerPlace.hpp \
	src/towers/Destroyer.hpp src/towers/Sniper.hpp \
	src/enemies/Puncher.hpp src/enemies/Sneaker.hpp \
	src/Player.hpp src/LevelEditor.hpp src/enemies/Tank.hpp
$(BUILD_DIR)/towers/TowerPlace.o: src/towers/TowerPlace.cpp src/towers/TowerPlace.hpp \
	src/towers/Tower.hpp src/GameObject.hpp \
	src/Config.hpp src/Coordinate.hpp \
	src/enemies/Enemy.hpp
